#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "missing parameter: <virtual env dir>"
    echo "usegae:"
    echo "$0 <virtual env dir>"
    echo "e.g:"
    echo "$0 ../../virtualenvs/seedlist/"
    exit 1
fi

virtual_env_dir=$1

. $virtual_env_dir/bin/activate
python ./import_investors_tsv.py tsv_data/investors.tsv
python ./import_journalists_tsv.py tsv_data/journalists.tsv
python ./import_startups_tsv.py tsv_data/startups.tsv
python ./import_vcs_tsv.py tsv_data/vc_funds.tsv
python ./import_accelerators_tsv.py tsv_data/accelerators.tsv


deactivate