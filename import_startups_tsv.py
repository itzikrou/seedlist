#!/usr/bin/env python
import argparse
import os

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "seedList.settings")
django.setup()

from configurations.models import ContactInformation, Location, Company
from startups.models import Startup


def fetch_string(data):
    if data:
        return data.strip().lower()
    else:
        return data
        
def parse_amount(str_amount):
    int_val = 0
    
    str_amount = str_amount.replace('$', '').replace(',', '')
    
    try:
        int_val = int(str_amount)
    except Exception:
        pass
    
    return int_val

def main(debug, fname):
    
    def parse_str_to_sum(s_range):
        if "K" in s_range or "k" in s_range:
            s_range = s_range.replace("K", "").replace("k", "")
            int_range = int(s_range)
            int_range = int_range * 1000
            return int_range
        
        if "M" in s_range or "m" in s_range:
            s_range = s_range.replace("M", "").replace("m", "")
            flt_range = float(s_range)
            flt_range = int(flt_range * 1000000)
            return flt_range
        
        return 0
        
    
    def parse_funding_range(funding_range):
        result_range = []
        funding_range = funding_range.replace('$', '')
        
        if "-" in funding_range:
            splited_range = funding_range.split('-')
            for s_range in splited_range:
                try:
                    result_range.append(parse_str_to_sum(s_range))
                except Exception:
                    print "could not parse %s" % s_range
        
        if len(result_range) == 2:
            return result_range[0], result_range[1]
        elif len(result_range) == 1:
            return result_range[0], result_range[0]
        else:
            return 0,0
    
    with open(fname) as f:
        for line in f:
            
            if debug:
                print line
            
            # TSV format
            splitted_line = line.split("\t")
            
            if debug:
                print splitted_line
                
            startup_name = fetch_string(splitted_line[0])
            if startup_name:
                startup_name = unicode(startup_name, errors='ignore')
                company, created = Company.objects.get_or_create(name = fetch_string(startup_name))
                
                kind = fetch_string(splitted_line[3])
                funding_range = fetch_string(splitted_line[4])
                from_funding_range, to_funding_range  = parse_funding_range(funding_range)
                desc = fetch_string(splitted_line[5])
                company_type = fetch_string(splitted_line[8])
                
                defaults = {
                            'kind' :kind,
                            'funding_range_from' :from_funding_range,
                            'funding_range_to' :to_funding_range,
                            'description' :desc,                            
                            'company_type' :company_type
                }
                
                startup, created = Startup.objects.get_or_create(company=company, defaults=defaults)

                contact_name = fetch_string(splitted_line[1])
                if contact_name:
                    contact_name = unicode(contact_name, errors='ignore')
                    contact_email = fetch_string(splitted_line[2])
                    defaults = {
                                'email' :contact_email
                    }
                    
                    contact, created = ContactInformation.objects.get_or_create(name=fetch_string(contact_name), defaults=defaults)
                    startup.contact.add(contact)
                
                location_name = fetch_string(splitted_line[6])
                if location_name:
                    location, created = Location.objects.get_or_create(name=fetch_string(location_name))
                    startup.location.add(location)
                
            else:
                print "skipping %s, line is invalid" % splitted_line
            


# "main" function:
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='imports startups tsv data into the project')
    parser.add_argument('-d', '--debug', dest='debug', action='store_true', default=False, help='print extra debug info')
    parser.add_argument('file_name', help='the startups file to parse')
    args = parser.parse_args()
    main(args.debug, args.file_name)
    

