from django.db import models

from configurations.models import MAX_CHAR_LENGTH, BaseModel, Company


class Multinational(BaseModel):
    company = models.ForeignKey(Company, null=True, help_text="Company")
    contact = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True)
    interest_areas = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True)
    when_to_approach = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True)

