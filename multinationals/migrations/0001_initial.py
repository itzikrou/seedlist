# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('configurations', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Multinational',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('last_updated', models.DateTimeField(help_text=b'When was field last updated (updates automatically)', auto_now=True, auto_now_add=True)),
                ('contact', models.CharField(max_length=1024, null=True, blank=True)),
                ('interest_areas', models.CharField(max_length=1024, null=True, blank=True)),
                ('when_to_approach', models.CharField(max_length=1024, null=True, blank=True)),
                ('company', models.ForeignKey(to='configurations.Company', help_text=b'Company', null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
    ]
