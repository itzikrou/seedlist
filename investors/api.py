import logging

from tastypie import fields
from tastypie.constants import ALL, ALL_WITH_RELATIONS

from configurations.api import ContactInformationResource, EntityResource, CompanyResource, ApproachTimeResource, \
    InvestorKindResource, IndustryResource, LocationResource
from investors.models import Investor
from util.api import BaseResource


logger = logging.getLogger(__name__)


class InvestorResource(BaseResource):
    contact = fields.ToOneField(ContactInformationResource, 'contact', full=True, null=True)
    entity = fields.ToOneField(EntityResource, 'entity', full=True, null=True)

    activity = fields.ToManyField(CompanyResource, 'activity', full=True, null=True)
    approach = fields.ToManyField(ApproachTimeResource, 'approach', full=True, null=True)
    kind = fields.ToManyField(InvestorKindResource, 'kind', full=True, null=True)
    primary_interest = fields.ToManyField(IndustryResource, 'primary_interest', full=True, null=True)
    other_interests = fields.ToManyField(IndustryResource, 'other_interests', full=True, null=True)
    location = fields.ToManyField(LocationResource, 'location', full=True, null=True)

    class Meta(BaseResource.Meta):
        queryset = Investor.objects.all().select_related('contact', 'entity') \
            .prefetch_related('activity', 'approach', 'kind', 'primary_interest', 'other_interests', 'location')\
            .order_by('contact__name')

        resource_name = 'investor'

        filtering = {
            'min_investment': ALL,
            'max_investment': ALL,
            'activity_level': ALL,
            'do_followons': ALL,
            'loved_companies': ALL,
            'do_convertible_note': ALL,
            'guidelines': ALL,

            'contact': ALL_WITH_RELATIONS,
            'activity': ALL_WITH_RELATIONS,
            'approach': ALL_WITH_RELATIONS,
            'entity': ALL_WITH_RELATIONS,
            'kind': ALL_WITH_RELATIONS,
            'primary_interest': ALL_WITH_RELATIONS,
            'other_interests': ALL_WITH_RELATIONS,
            'location': ALL_WITH_RELATIONS,
        }

        ordering = ['contact', 'contact__name','last_updated']
