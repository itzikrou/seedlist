from django.db import models

from configurations.models import MAX_CHAR_LENGTH, ApproachTime, InvestorKind, \
    ActivityLevel, Location, ContactInformation, Industry, Company, Entity, \
    BaseModel


class Investor(BaseModel):
    min_investment = models.IntegerField(default=0, help_text="Min investment [$USD]")
    max_investment = models.IntegerField(default=0, help_text="Max investment [$USD]")
    activity_level = models.CharField(max_length=MAX_CHAR_LENGTH, choices=ActivityLevel.ACTIVITY_LEVEL,
                                      default=ActivityLevel.NONE, blank=True, null=True,
                                      help_text="Activity level in Israel")
    do_followons = models.BooleanField(default=False, help_text="Do follow-ons?")
    loved_companies = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True,
                                       help_text="Companies they love to invest in")
    do_convertible_note = models.BooleanField(default=False, help_text="Do convertible note")
    guidelines = models.TextField(blank=True, null=True,
                                  help_text="Guidelines from the investor. In this column only the investors themselves should provide guidelines about what they would like to receive and in what format. Entrepreneurs - please DO NOT change this column")


    contact = models.ForeignKey(ContactInformation, null=True, help_text="Contact Deatils")
    activity = models.ManyToManyField(Company, null=True,
                                      help_text="Number of investors investment in the last 12 months")
    approach = models.ManyToManyField(ApproachTime, null=True, help_text="When should you approach the investor")
    entity = models.ForeignKey(Entity, null=True, help_text="Entity")
    kind = models.ManyToManyField(InvestorKind, null=True, help_text="Type of investor")
    primary_interest = models.ManyToManyField(Industry, null=True, related_name='primary_interest',
                                              help_text="Primary interest industry [Make sure you fit!]")
    other_interests = models.ManyToManyField(Industry, null=True, related_name='other_interests',
                                             help_text="Other interests industry")
    location = models.ManyToManyField(Location, null=True, help_text="Location")
