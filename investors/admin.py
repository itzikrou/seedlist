import locale

from django.contrib import admin
from django.utils.html import format_html

from configurations.admin import BaseAdmin
from investors.models import Investor


class InvestorAdmin(BaseAdmin):
    
    list_display = ('name', 'activity_summary', 'approach_summary', 'entity', 'kind_summary',
                    'primary_interest_summary', 'other_interests_summary', 'custom_min_investment', 'custom_max_investment',
                    'activity_level', 'custom_email', 'do_followons', 'location_summary', 'loved_companies',
                    'do_convertible_note', 'guidelines', 'last_updated')
    
    list_filter = ['activity_level', 'do_followons', 'do_convertible_note', 'kind', 'min_investment', 'max_investment', 'location', 'primary_interest', 'other_interests']
    
    search_fields = ['contact__name', 'contact__email', 'kind__name', 'activity__name', 'entity__name', 'primary_interest__name', 'location__name']
    
    filter_horizontal = ('activity', 'approach', 'kind', 'primary_interest', 'other_interests', 'location')
    
    readonly_fields = ('last_updated',)
    ordering = ('-last_updated',)
    
    def get_queryset(self, request):
        """
        Overloading of the Django default function - this is used in order to do select releated of the following models and reduce SQL queries
        """
        qs = super(InvestorAdmin, self).get_queryset(request).select_related("contact", "entity")\
        .prefetch_related('activity', 'approach', 'kind', 'primary_interest', 'other_interests', 'location')
        
        return qs
    
    def name(self, obj):        
        return obj.contact.name
    
    name.admin_order_field = 'contact__name'

    def activity_summary(self, obj):
        activity_list = obj.activity.values_list("name", flat=True)
        return " ".join(activity_list)
    
    activity_summary.short_description = 'Activity'
    activity_summary.admin_order_field = 'activity__name'


    def custom_email(self, obj):
        if obj.contact.email:
            email_link = "<a href='mailto:{}'>{}</a>"
            return format_html(email_link , obj.contact.email, obj.contact.email)
        else:
            return obj.contact.email
    
    custom_email.allow_tags = True
    custom_email.short_description = 'Email'
    custom_email.admin_order_field = 'contact__email'

    def approach_summary(self, obj):
        approach_list = obj.approach.values_list("name", flat=True)
        return " ".join(approach_list)

    approach_summary.short_description = 'Approach time'
    approach_summary.admin_order_field = 'approach__name'
    
    def entity(self, obj):        
        return obj.entity.name

    def kind_summary(self, obj):
        kind_list = obj.kind.values_list("name", flat=True)
        return " ".join(kind_list)
    
    kind_summary.short_description = 'Kind'
    kind_summary.admin_order_field = 'kind__name'
    
    def primary_interest_summary(self, obj):
        primary_interest_list = obj.primary_interest.values_list("name", flat=True)
        return " ".join(primary_interest_list)
    
    primary_interest_summary.short_description = 'Primary Interest'
    primary_interest_summary.admin_order_field = 'primary_interest__name'
        

    def other_interests_summary(self, obj):
        other_interests_list = obj.other_interests.values_list("name", flat=True)
        return " ".join(other_interests_list)
    
    other_interests_summary.short_description = 'Other Interests'
    other_interests_summary.admin_order_field = 'other_interests__name'
    
    def location_summary(self, obj):
        location_list = obj.location.values_list("name", flat=True)
        return " ".join(location_list)
    
    location_summary.short_description = 'Location'
    location_summary.admin_order_field = 'location__name'
    
    def custom_min_investment(self, obj):
        
        return locale.currency( obj.min_investment, grouping=True )

    custom_min_investment.short_description = 'Min Investment [USD]'
    custom_min_investment.admin_order_field = 'min_investment'


    def custom_max_investment(self, obj):
        
        return locale.currency( obj.max_investment, grouping=True )

    custom_max_investment.short_description = 'MAX Investment [USD]'
    custom_max_investment.admin_order_field = 'max_investment'

    
admin.site.register(Investor, InvestorAdmin)
