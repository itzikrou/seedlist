# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('configurations', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Investor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('last_updated', models.DateTimeField(help_text=b'When was field last updated (updates automatically)', auto_now=True, auto_now_add=True)),
                ('min_investment', models.IntegerField(default=0, help_text=b'Min investment [$USD]')),
                ('max_investment', models.IntegerField(default=0, help_text=b'Max investment [$USD]')),
                ('activity_level', models.CharField(default=b'Non-Existent', choices=[(b'High', b'High'), (b'Medium', b'Medium'), (b'Low', b'Low'), (b'Non-Existent', b'Non-Existent')], max_length=1024, blank=True, help_text=b'Activity level in Israel', null=True)),
                ('do_followons', models.BooleanField(default=False, help_text=b'Do follow-ons?')),
                ('loved_companies', models.CharField(help_text=b'Companies they love to invest in', max_length=1024, null=True, blank=True)),
                ('do_convertible_note', models.BooleanField(default=False, help_text=b'Do convertible note')),
                ('guidelines', models.TextField(help_text=b'Guidelines from the investor. In this column only the investors themselves should provide guidelines about what they would like to receive and in what format. Entrepreneurs - please DO NOT change this column', null=True, blank=True)),
                ('activity', models.ManyToManyField(help_text=b'Number of investors investment in the last 12 months', to='configurations.Company', null=True)),
                ('approach', models.ManyToManyField(help_text=b'When should you approach the investor', to='configurations.ApproachTime', null=True)),
                ('contact', models.ForeignKey(to='configurations.ContactInformation', help_text=b'Contact Deatils', null=True)),
                ('entity', models.ForeignKey(to='configurations.Entity', help_text=b'Entity', null=True)),
                ('kind', models.ManyToManyField(help_text=b'Type of investor', to='configurations.InvestorKind', null=True)),
                ('location', models.ManyToManyField(help_text=b'Location', to='configurations.Location', null=True)),
                ('other_interests', models.ManyToManyField(help_text=b'Other interests industry', related_name='other_interests', null=True, to='configurations.Industry')),
                ('primary_interest', models.ManyToManyField(help_text=b'Primary interest industry [Make sure you fit!]', related_name='primary_interest', null=True, to='configurations.Industry')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
    ]
