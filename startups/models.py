from django.db import models

from configurations.models import MAX_CHAR_LENGTH, Location, ContactInformation, \
    Company, BaseModel


DESC_LENGTH=140

class Startup(BaseModel):
    company = models.ForeignKey(Company, null=True, help_text="Company")
    contact = models.ManyToManyField(ContactInformation, null=True, help_text="Contact person")
    kind = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True, help_text="Kind")
    funding_range_from = models.IntegerField(default=0, help_text="Funding range: min amount")
    funding_range_to = models.IntegerField(default=0, help_text="Funding range: max amount")
    description = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True, help_text="Description (140 chars)")
    location = models.ManyToManyField(Location, null=True, help_text="Location")
    company_type = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True, help_text="Company type")

