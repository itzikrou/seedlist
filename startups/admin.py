import locale

from django.contrib import admin
from django.utils.html import format_html

from configurations.admin import BaseAdmin
from startups.models import Startup


class StartupAdmin(BaseAdmin):
    
    list_display = ('company', 'contact_person', 'custom_email', 'kind', 'custom_funding_range_from', 
                    'custom_funding_range_to', 'description', 'location_summary', 'company_type', 'last_updated')
    
    list_filter = ['funding_range_from', 'funding_range_to', 'location', 'kind']
    
    search_fields = ['kind', 'funding_range_from', 'funding_range_to', 'description']
    
    filter_horizontal = ('location', 'contact')
    
    readonly_fields = ('last_updated',)
    ordering = ('-last_updated',)
    
    def get_queryset(self, request):
        """
        Overloading of the Django default function - this is used in order to do select releated of the following models and reduce SQL queries
        """
        qs = super(StartupAdmin, self).get_queryset(request).select_related("company").prefetch_related('location', 'contact')
        
        return qs
    
    def contact_person(self, obj):
        name_list = obj.contact.values_list("name", flat=True)
        return " ".join(name_list)
    
    contact_person.admin_order_field = 'contact__name'

    def custom_email(self, obj):
        
        email_list = obj.contact.values_list("email", flat=True)
        result = []
        for email in email_list:
            email_link = "<a href='mailto:{}'>{}</a>"
            result.append(format_html(email_link , email, email))
        
        return " ".join(result)
    
    custom_email.allow_tags = True
    custom_email.short_description = 'Email'
    custom_email.admin_order_field = 'contact__email'
    
    def location_summary(self, obj):
        location_list = obj.location.values_list("name", flat=True)
        return " ".join(location_list)
    
    location_summary.short_description = 'Location'
    location_summary.admin_order_field = 'location__name'
    
    def custom_funding_range_from(self, obj):
        
        return locale.currency( obj.funding_range_from, grouping=True )

    custom_funding_range_from.short_description = 'Funding range: from'
    custom_funding_range_from.admin_order_field = 'funding_range_from'


    def custom_funding_range_to(self, obj):
        
        return locale.currency( obj.funding_range_to, grouping=True )

    custom_funding_range_to.short_description = 'Funding range: to'
    custom_funding_range_to.admin_order_field = 'funding_range_to'
    
    


admin.site.register(Startup, StartupAdmin)