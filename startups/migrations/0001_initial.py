# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('configurations', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Startup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('last_updated', models.DateTimeField(help_text=b'When was field last updated (updates automatically)', auto_now=True, auto_now_add=True)),
                ('kind', models.CharField(help_text=b'Kind', max_length=1024, null=True, blank=True)),
                ('funding_range_from', models.IntegerField(default=0, help_text=b'Funding range: min amount')),
                ('funding_range_to', models.IntegerField(default=0, help_text=b'Funding range: max amount')),
                ('description', models.CharField(help_text=b'Description (140 chars)', max_length=1024, null=True, blank=True)),
                ('company_type', models.CharField(help_text=b'Company type', max_length=1024, null=True, blank=True)),
                ('company', models.ForeignKey(to='configurations.Company', help_text=b'Company', null=True)),
                ('contact', models.ManyToManyField(help_text=b'Contact person', to='configurations.ContactInformation', null=True)),
                ('location', models.ManyToManyField(help_text=b'Location', to='configurations.Location', null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
    ]
