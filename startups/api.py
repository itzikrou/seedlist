import logging

from tastypie import fields
from tastypie.constants import ALL, ALL_WITH_RELATIONS

from configurations.api import ContactInformationResource, CompanyResource, LocationResource
from startups.models import Startup
from util.api import BaseResource


logger = logging.getLogger(__name__)


class StartupResource(BaseResource):
    company = fields.ToOneField(CompanyResource, 'company', full=True, null=True)

    contact = fields.ToManyField(ContactInformationResource, 'contact', full=True, null=True)
    location = fields.ToManyField(LocationResource, 'location', full=True, null=True)

    class Meta(BaseResource.Meta):
        queryset = Startup.objects.all().select_related('company') \
            .prefetch_related('contact', 'location')

        resource_name = 'startup'

        filtering = {
            'kind': ALL,
            'funding_range_from': ALL,
            'funding_range_to': ALL,
            'description': ALL,
            'company_type': ALL,

            'location': ALL_WITH_RELATIONS,
            'company': ALL_WITH_RELATIONS,
            'contact': ALL_WITH_RELATIONS
        }

        ordering = ['company', 'contact', 'kind', 'funding_range_from', 'funding_range_to', 'description', 'location',
                    'company_type', 'last_updated' ]
