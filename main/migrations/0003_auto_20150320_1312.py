# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User

from django.db import models, migrations


def enter_data(apps, schema_editor):
    SeedListUser = apps.get_model("main", "SeedListUser")
    defaults = {
        'is_staff': True,
        'is_superuser': True,
    }
    guest, created = SeedListUser.objects.get_or_create(username='guest@seedLists.com', defaults=defaults)

    guest = User.objects.get(username='guest@seedLists.com')
    guest.set_password('guest')
    guest.save()

class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_seedlistuser_last_updated'),
    ]

    operations = [
        migrations.RunPython(enter_data),
    ]
