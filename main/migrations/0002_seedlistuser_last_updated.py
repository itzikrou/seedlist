# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='seedlistuser',
            name='last_updated',
            field=models.DateTimeField(auto_now_add=True, default=datetime.datetime(2015, 3, 20, 13, 2, 38, 311296, tzinfo=utc), auto_now=True, help_text=b'When was field last updated (updates automatically)'),
            preserve_default=False,
        ),
    ]
