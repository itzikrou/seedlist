# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
        ('configurations', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='LinkedInConnection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('last_updated', models.DateTimeField(help_text=b'When was field last updated (updates automatically)', auto_now=True, auto_now_add=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='LinkedInInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('last_updated', models.DateTimeField(help_text=b'When was field last updated (updates automatically)', auto_now=True, auto_now_add=True)),
                ('linkedin_id', models.CharField(null=True, max_length=1024, blank=True, help_text=b'linkedin_id', unique=True, db_index=True)),
                ('firstName', models.CharField(help_text=b'firstName', max_length=1024, null=True, blank=True)),
                ('lastName', models.CharField(help_text=b'lastName', max_length=1024, null=True, blank=True)),
                ('headline', models.CharField(help_text=b'headline', max_length=1024, null=True, blank=True)),
                ('industry', models.CharField(help_text=b'industry', max_length=1024, null=True, blank=True)),
                ('siteStandardProfileRequest', models.CharField(help_text=b'siteStandardProfileRequest', max_length=1024, null=True, blank=True)),
                ('pictureUrl', models.CharField(help_text=b'pictureUrl', max_length=1024, null=True, blank=True)),
                ('apiStandardProfileRequest', models.CharField(help_text=b'apiStandardProfileRequest', max_length=1024, null=True, blank=True)),
                ('publicProfileUrl', models.CharField(help_text=b'publicProfileUrl', max_length=1024, null=True, blank=True)),
                ('connections', models.ManyToManyField(to='main.LinkedInInfo', through='main.LinkedInConnection')),
                ('location', models.ForeignKey(to='configurations.Location', help_text=b'Location', null=True)),
                ('positions', models.ManyToManyField(help_text=b'positions', to='configurations.Company', null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SeedListUser',
            fields=[
                ('user_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('linkedin_id', models.CharField(null=True, max_length=1024, blank=True, help_text=b'linked-in profile id', unique=True, db_index=True)),
                ('contact_info', models.ForeignKey(to='configurations.ContactInformation', help_text=b'contact information', null=True)),
                ('linkedin_info', models.ForeignKey(to='main.LinkedInInfo', help_text=b'linkedin information', null=True)),
            ],
            options={
                'abstract': False,
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
            },
            bases=('auth.user',),
        ),
        migrations.AddField(
            model_name='linkedinconnection',
            name='linkedin_info_1',
            field=models.ForeignKey(related_name='linkedin_info_1', to='main.LinkedInInfo', help_text=b'linkedin information', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='linkedinconnection',
            name='linkedin_info_2',
            field=models.ForeignKey(related_name='linkedin_info_2', to='main.LinkedInInfo', help_text=b'linkedin information', null=True),
            preserve_default=True,
        ),
    ]
