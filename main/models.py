from django.contrib.auth.models import User
from django.db import models

from configurations.models import MAX_CHAR_LENGTH, ContactInformation, BaseModel, Location, Company


class LinkedInInfo(BaseModel):
    linkedin_id = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True, help_text="linkedin_id", db_index=True, unique=True)
    firstName = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True, help_text="firstName")
    lastName = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True, help_text="lastName")
    headline = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True, help_text="headline")
    industry = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True, help_text="industry")
    siteStandardProfileRequest = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True, help_text="siteStandardProfileRequest")
    pictureUrl = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True, help_text="pictureUrl")
    apiStandardProfileRequest = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True, help_text="apiStandardProfileRequest")
    publicProfileUrl = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True, help_text="publicProfileUrl")

    location = models.ForeignKey(Location, null=True, help_text="Location")
    positions = models.ManyToManyField(Company, null=True, help_text="positions")

    connections = models.ManyToManyField('self', through='LinkedInConnection', symmetrical=False)


class SeedListUser(User, BaseModel):
    linkedin_id = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True, db_index=True, unique=True, help_text="linked-in profile id")
    contact_info = models.ForeignKey(ContactInformation, null=True, help_text="contact information")
    linkedin_info = models.ForeignKey(LinkedInInfo, null=True, help_text="linkedin information")

class LinkedInConnection(BaseModel):
    linkedin_info_1 = models.ForeignKey(LinkedInInfo, null=True, help_text="linkedin information", related_name='linkedin_info_1')
    linkedin_info_2 = models.ForeignKey(LinkedInInfo, null=True, help_text="linkedin information", related_name='linkedin_info_2')




