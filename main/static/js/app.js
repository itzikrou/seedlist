// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs

function hoverImg(hoverObj){

    var $hoverObj = $(hoverObj);
    var img_src = $hoverObj.attr("src");
    var img_array = img_src.split(".png");

    $hoverObj.attr("src", img_array[0] + "_hover" + ".png");

}

function normalImg(hoverObj){

    var $hoverObj = $(hoverObj);
    var img_src = $hoverObj.attr("src");
    var img_array = img_src.split(".png");
    var img_array = img_array[0].split("_hover");

    $hoverObj.attr("src", img_array[0] + ".png");

}

String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

String.prototype.trunc = String.prototype.trunc ||
      function(n){
          return this.length>n ? this.substr(0,n-1)+'&hellip;' : this;
};

function query_string() {
  // This function is anonymous, is executed immediately and 
  // the return value is assigned to QueryString!
  var query_string = {};
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
        // If first entry with this name
    if (typeof query_string[pair[0]] === "undefined") {
      query_string[pair[0]] = pair[1];
        // If second entry with this name
    } else if (typeof query_string[pair[0]] === "string") {
      var arr = [ query_string[pair[0]], pair[1] ];
      query_string[pair[0]] = arr;
        // If third or later entry with this name
    } else {
      query_string[pair[0]].push(pair[1]);
    }
  } 
    return query_string;
}

$(document).foundation();

$(document).ready(function(){
	http_handler = new HttpHandler("api/v1/");
	$('.spin').spin();
	$('.spin').spin('hide');

    user_service = new UserService(new HttpHandler(API_VERSION), "/user/");
});

