UserService.prototype.linkedin_done_loading = function(){

    // console.log("done loading - login");
    IN.Event.on(IN, "auth", user_service.login_with_linkedin);

}

$( document ).ready(function() {

	user_service = new UserService(new HttpHandler(API_VERSION), "/user/");

	$('#login_form')
	  .on('invalid.fndtn.abide', function(e) {

	    var invalid_fields = $(this).find('[data-invalid]');
	    // console.log(invalid_fields);

	  })
	  .on('valid.fndtn.abide', function(e) {

            // foundation abide event is triggered twice
            if(e.namespace != 'abide.fndtn') {
                return;
            }

	      // console.log('valid!');
		  user_service.signup();
	  });

});