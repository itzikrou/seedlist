function HttpHandler(api_prefix) {

	this.api_prefix = api_prefix;
	$.ajaxSetup({
		  contentType: "application/json; charset=utf-8",
		  processData:false,
		  timeout:1000*60
		});
}

HttpHandler.prototype.generic_success = function(msg){
	this._toaster("success", msg);
}

HttpHandler.prototype.generic_failure = function(msg){
	this._toaster("error", msg);
}

HttpHandler.prototype._toaster = function(toast_type, msg){
	
	if(typeof(msg) == "undefined" || msg == null){
		if(toast_type == "success"){
			msg = "Success!";
		}
		else{
			msg = "Error!";
		}
	}
	
	toastr[toast_type](msg)

	toastr.options = {
	  "closeButton": true,
	  "debug": false,
	  "progressBar": true,
	  "positionClass": "toast-top-right",
	  "onclick": null,
	  "showDuration": "300",
	  "hideDuration": "1000",
	  "timeOut": "2000",
	  "extendedTimeOut": "1000",
	  "showEasing": "swing",
	  "hideEasing": "linear",
	  "showMethod": "fadeIn",
	  "hideMethod": "fadeOut"
	}	
}


HttpHandler.prototype.build_url = function(url, url_params, req_type){
	
	var built_url = this.api_prefix + url;
	
	if(url_params != null) {
		
		for(var key in url_params) {
			
			if (url_params.hasOwnProperty(key)) {
				
				if(built_url.indexOf("?") == -1){
					built_url = built_url + "?";
				}
				else{
					built_url = built_url + "&";
				}
				
				built_url = built_url + key + "=" + url_params[key];
			 } 
		}
	}

	if(req_type == "post" || req_type == "put"){
		if(!built_url.endsWith("/")){
			built_url = built_url  + "/";
		}
	}
	
	return built_url;
}

HttpHandler.prototype.get = function(callback_obj, url, url_params, success_callback_method_name, faliure_callback_method_name, with_loading) {
	
	if(typeof(success_callback_method_name) == "undefined" || 
			success_callback_method_name == null || 
			typeof(callback_obj[success_callback_method_name]) == "undefined"){
		
		console.log("missing callback method. not making the GET request.");
		return;
	}
	
	var request_url = this.build_url(url, url_params, "get");
	console.log("making a GET request to " + request_url);
	
	if(with_loading != false) {

        $('.spin').spin('show');
        $("#spinner_global_container").show();

	}

	$.ajax({
		  type: "GET",
		  url: request_url,
		  beforeSend: function (request)
          {
              request.setRequestHeader("API_REQUEST", true);
          }
		})
	.done(function(data) {
			callback_obj[success_callback_method_name](data, callback_obj);	
	  })
    .fail(function(jqXHR, textStatus, errorThrown) {
    	
    	console.log("request failed.");
    	console.log(jqXHR);
    	
    	if(typeof(faliure_callback_method_name) != "undefined" && 
    			faliure_callback_method_name != null &&
    			faliure_callback_method_name != "" &&
    			typeof(callback_obj[faliure_callback_method_name]) != "undefined") {
    		
    		callback_obj[faliure_callback_method_name](jqXHR, callback_obj);
    	}
    })
    .always(function(responseObj, textStatus, httpObj) {
        $("#spinner_global_container").hide();
        $('.spin').spin('hide');
    });

}

HttpHandler.prototype._make_write_request = function(call_type, callback_obj, url, url_params, data, success_callback_method_name, faliure_callback_method_name, with_loading) {
	
	if(typeof(success_callback_method_name) == "undefined" || 
			success_callback_method_name == null || 
			typeof(callback_obj[success_callback_method_name]) == "undefined"){
		
		console.log("missing callback method. not making the " + call_type + " request.");
		return;
	}
	
	var request_url = this.build_url(url, null, call_type.toLowerCase());
	var stringified_data = JSON.stringify(data);
	
	console.log("making a " + call_type + " request to " + request_url);
	// console.log("making a " + call_type + " request to " + request_url + " with data: " + stringified_data);

	if(with_loading != false) {

        $('.spin').spin('show');
        $("#spinner_global_container").show();

	}

	$.ajax({
		  type: call_type,
		  url: request_url,
		  data: stringified_data,
		  beforeSend: function (request)
          {
              request.setRequestHeader("API_REQUEST", true);
          }
		})
		.done(function(data) {
			callback_obj[success_callback_method_name](data, callback_obj);	
	    })
	    .fail(function(jqXHR, textStatus, errorThrown) {
	    	
	    	console.log("request failed.");
	    	console.log(jqXHR);
	    	
	    	if(typeof(faliure_callback_method_name) != "undefined" && 
	    			faliure_callback_method_name != null &&
	    			faliure_callback_method_name != "" &&
	    			typeof(callback_obj[faliure_callback_method_name]) != "undefined") {
	    		
	    		callback_obj[faliure_callback_method_name](jqXHR, callback_obj);
	    	}
	    })
	    .always(function(responseObj, textStatus, httpObj) {
            $("#spinner_global_container").hide();
            $('.spin').spin('hide');
	    });

	
}

HttpHandler.prototype.post = function(callback_obj, url, url_params, data, success_callback_method_name, faliure_callback_method_name, with_loading) {
	
	this._make_write_request("POST", callback_obj, url, url_params, data, success_callback_method_name, faliure_callback_method_name, with_loading);
}

HttpHandler.prototype.put = function(callback_obj, url, url_params, data, success_callback_method_name, faliure_callback_method_name, with_loading) {
	
	this._make_write_request("PUT", callback_obj, url, url_params, data, success_callback_method_name, faliure_callback_method_name, with_loading);
}