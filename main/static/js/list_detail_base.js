UserService.prototype.linkedin_done_loading = function(){

    // console.log("done loading - main");
    IN.Event.on(IN, "auth", user_service.get_profile_data);
}

function list_modal_details_changer(){

    if(Modernizr.mq('only screen and (min-width: 64.063em)')){
        // large screens
        // console.log("large display");
        $("#detailed_view_container_id").removeClass("reveal-modal");
        $("#detailed_view_container_id").addClass("show-for-large-up");
        $("#detailed_view_container_id").addClass("detailed_view_container");
        $("#detailed_view_container_id").addClass("max_height_scroll");
        $(".detailed_view_body_contents").addClass("max_height_scroll");
        $(".summ_list_container").addClass("max_height_scroll");
        $("#detailed_view_container_id").attr("style", "");

        $(".close-reveal-modal").remove();
        $(".summ_list_item").unbind('click');
        $(".summ_list_item").prop("onclick", LDService.prototype.render_row);

    }
    else{
        // other displays
        // console.log("not large displays");
        $("#detailed_view_container_id").addClass("reveal-modal");
        $("#detailed_view_container_id").removeClass("show-for-large-up");
        $("#detailed_view_container_id").removeClass("max_height_scroll");
        $(".detailed_view_body_contents").removeClass("max_height_scroll");
        $(".summ_list_container").removeClass("max_height_scroll");

        $("#detailed_view_container_id").append('<a class="close-reveal-modal">&#215;</a>');
        $(".summ_list_item").on('click', function(){
            if($("#no_results").length == 0){
                $('#detailed_view_container_id').foundation('reveal', 'open');
            }
        });
    }
}

$( document ).ready(function() {

    $( window ).resize(function() {
        list_modal_details_changer();
    });

});
