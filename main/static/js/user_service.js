// class constructor
function UserService(http_handler, api_path) {

	this.http_handler = http_handler;
	this.api_path = api_path;
    this.data_fields = ["id", "firstName", "lastName", "email-address",
                       "headline", "summary", "location", "industry",
                       "positions", "picture-url", "site-standard-profile-request",
                       "api-standard-profile-request", "public-profile-url"];

}

// methods definition
UserService.prototype.login = function(linkedin_id) {

	var req_data = {
			"username" : $("#email_input").val(),
			"password" : $("#password_input").val(),
			"linkedin_id" : linkedin_id
	};

	var url_params = {};
	this.http_handler.post(this, this.api_path + "login", url_params, req_data, "login_on_success", "request_error");
};

UserService.prototype.login_on_success = function(data) {
	// redirect after login
	var next = query_string().next;
	if(typeof(next) == 'undefined' || next == null || next == ""){
		next = '/';
	}

	window.location.replace(next);
};


UserService.prototype.show_error  = function(msg) {
    var $error = $("#signup_error");
    $error.html(msg);
    $error.attr("style", "display: block;");
    $error.show();
};

UserService.prototype.signup  = function() {

	var password1 = $("#password_input").val();
    var password2 = $("#password_input_again").val();

    if(password1 != password2){
        this.show_error("passwords do not match..");
        return;
    }

    var req_data = {
        "username" : $("#email_input").val(),
        "password" : password1,
        "first_name" : $("#first_name_input").val(),
        "last_name" : $("#last_name_input").val()
	};

	var url_params = {};
	this.http_handler.post(this, this.api_path + "register", url_params, req_data, "login_on_success", "request_error");
};

UserService.prototype.request_error  = function(data) {
    var msg = "some unknown error has occurred. Please let us know...";

    if(typeof data != "undefined" &&
        typeof data.responseJSON != "undefined"){
        msg = data.responseJSON.error_message;
    }

    this.show_error(msg);
};

UserService.prototype.anonymous= function() {

	var req_data = {
			"username" : "guest@seedLists.com",
			"password" : "guest",
			"linkedin_id" : null
	};

	var url_params = {};
	this.http_handler.post(this, this.api_path + "login", url_params, req_data, "login_on_success");
};

UserService.prototype.logout = function(click_obj) {

	// logout of Linkedin
    if(typeof IN != "undefined" &&
        typeof IN.User != "undefined" &&
        IN.User.isAuthorized() == true){

        $('.spin').spin('show');
        $("#spinner_global_container").show();

        IN.User.logout(this.logout_success_callback, this);
    }
    else{
        this.logout_success_callback();
    }
};

UserService.prototype.logout_success_callback = function(obj) {
    $("#spinner_global_container").hide();
    $('.spin').spin('hide');

    console.log("linkedin logout success");
    var req_data = {};
	var url_params = {};
	this.http_handler.post(this, this.api_path + "logout", url_params, req_data, "logout_on_success");

};

UserService.prototype.logout_on_success = function(data) {

	window.location.replace('/');
};


UserService.prototype.get_profile_data = function() {

	// console.log("get_profile_data");

    IN.API.Profile("me")
      .fields(user_service.data_fields)
      .result(function(result) {
      	user_service.get_profile_data_on_success(result);
    });
};

UserService.prototype.get_profile_data_on_success = function(data) {
	
	//console.log(data);
	this.http_handler.post(this, this.api_path + "update_from_linkedin", {}, data, "update_from_linkedin_on_success", "", false);
};

UserService.prototype.get_profile_connections_on_success = function(data) {

	//console.log(data);
	if(typeof data != "undefined" &&
        data != null){
        data['user_linkedin_id'] = IN.User.getMemberId();
    }
    this.http_handler.post(this, this.api_path + "update_connections_from_linkedin", {}, data, "update_connections_from_linkedin_on_success", "", false);
};

UserService.prototype.update_from_linkedin_on_success = function(data) {
	// console.log(data);
    IN.API.Connections("me").fields(user_service.data_fields).result(function(result) {
      	user_service.get_profile_connections_on_success(result);
    });
};

UserService.prototype.update_connections_from_linkedin_on_success = function(data) {
	console.log("finished updating all profile data from linkedin");
};

UserService.prototype.login_with_linkedin = function(){
    
    // console.log("login_with_linkedin");
    user_service.login(IN.User.getMemberId());
};

UserService.prototype.login_with_linkedin_onclick = function(click_obj){

    IN.User.authorize(user_service.login_with_linkedin);
};