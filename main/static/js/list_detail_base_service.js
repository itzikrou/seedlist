// class constructor
function LDService(http_handler, api_path, order_by) {

	this.http_handler = http_handler;
	this.api_path = api_path;
	this.objects = null;
	this.meta = null;
	this.filter_by = {};
    this.order_by = order_by;
}

// methods definition

LDService.prototype.filter = function(clickObj) {
    var $text_inputs = $(".filter_bar input[type='text']");
    var filter_by = {};

    for(var i=0; i< $text_inputs.length; i++){

        var $curr_input = $($text_inputs[i]);
        var input_val = $curr_input.val();

        if(typeof input_val != "undefined" &&
            input_val != null &&
            input_val != ""){

                var input_filter_key = $curr_input.data().filter_key;
                filter_by[input_filter_key] = input_val;
        }

    }

    var $radio_inputs = $(".filter_bar input[type='radio']");
    for(var i=0; i< $radio_inputs.length; i++){

        var $curr_input = $($radio_inputs[i]);

        if($curr_input.is(':checked')) {

            var input_val = $curr_input.val();

            if(typeof input_val != "undefined" &&
                input_val != null &&
                input_val != ""){

                    var input_filter_key = $curr_input.data().filter_key;
                    filter_by[input_filter_key] = input_val;
            }

        }
    }


    var $select_inputs = $(".filter_bar select option:selected");

    for(var i=0; i< $select_inputs.length; i++){

        var $curr_input = $($select_inputs[i]);

        var input_val = $curr_input.val();

        if(typeof input_val != "undefined" &&
            input_val != null &&
            input_val != ""){

                var input_filter_key = $curr_input.data().filter_key;
                filter_by[input_filter_key] = input_val;
        }

    }

    this.filter_by = filter_by;
    // console.log(filter_by);
    this.get();
};

LDService.prototype.load_more = function(clickObj) {
    // investor_service.meta.next
    this.get();
};

LDService.prototype.reset = function(clickObj) {
    this.filter_by = {};
    this.get();
};

LDService.prototype.set_order_by = function(changeObj) {
    var $selected_order_by = $($(changeObj).children("option:selected"));
    var order_by_order = $("#order_by_order").data('value');
    // console.log(order_by_order);
    if(order_by_order == 'ASC'){
        this.order_by = {'order_by' : $selected_order_by.val()};
    }
    else{
        this.order_by = {'order_by' : '-' + $selected_order_by.val()};
    }

    this.get();
};

LDService.prototype.change_order_by_order = function(clickobj) {
    var order_by_order = $(clickobj).data('value');
    // console.log(order_by_order);
    if(order_by_order == 'DESC'){
        $("#order_by_order").data('value','ASC');
        $("#order_by_order").html('ASC');
    }
    else{
        $("#order_by_order").data('value','DESC');
        $("#order_by_order").html('DESC');
    }

    this.set_order_by($("#order_by_select_box"));
};


LDService.prototype.get = function(url) {

    if(typeof url != "undefined" && url != null && url != ""){
        this.http_handler.get(this, url, "get_on_success");
    }

	var request_params =  $.extend(true,{},this.filter_by);
    if(!_.isEmpty(this.order_by)){
        request_params = $.extend(request_params,this.order_by)
    }
	this.http_handler.get(this, this.api_path, request_params, "get_on_success");
};

LDService.prototype.get_on_success = function(data) {
	
	// console.log(data);

	this.objects = data.objects;
	this.meta = data.meta;

    var template = $("#list_detail_view_filter_bar").html();
    var compiled = _.template(template);
	var compilted_html = compiled(this);
	$("#filter_bar_container").html(compilted_html);

    var template = $("#list_detail_view_sort_bar").html();
    var compiled = _.template(template);
	var compilted_html = compiled(this);
	$("#sort_bar_container").html(compilted_html);


	// TODO: handle an empty object list
    if(typeof data.objects != "undefined" &&
        data.objects != null &&
        data.objects.length > 0){
        var template = $("#list_detail_view_contents").html();
        var compiled = _.template(template);
        var compilted_html = compiled(data);
        $("#contents_container").html(compilted_html);
        this.render_obj_details(data.objects[0]);
    }
    else{
        var template = $("#list_detail_view_empty_contents").html();
        var compiled = _.template(template);
        var compilted_html = compiled({});
        $("#contents_container").html(compilted_html);
        this.render_obj_details({}, true);
    }

    list_modal_details_changer();
};

LDService.prototype.render_obj_details = function(obj, is_empty) {

    var template = $("#list_detail_view_detail").html();

    if(typeof is_empty != "undefined" && is_empty == true){
        template = $("#list_detail_view_empty_detail").html();
    }

    var compiled = _.template(template);
	var compilted_html = compiled(obj);
	$(".detailed_view_container").html(compilted_html);

    if(typeof is_empty != "undefined" && is_empty == true){
        var $obj = $(".detailed_view_header_contents_black");
        $obj.removeClass("detailed_view_header_contents_black");
        $obj.addClass("detailed_view_header_contents_white");
    }


};

LDService.prototype.get_named_list_items = function(field_name) {
	var field_str = "";
	if(typeof(field_name) != "undefined" && field_name != null){
        // console.log(field_name);
		for(var i=0; i<field_name.length; i++){
			field_str += field_name[i].name + ",";
		}

	}
	if(field_str.endsWith(',')){
		field_str = field_str.substring(0, field_str.length - 1);
	}

	if(field_str == ""){
		field_str = "unknown";
	}
	return field_str;
};

LDService.prototype.get_object_by_id = function(id) {
    if(typeof this.objects != "undefined" &&
        this.objects != null &&
        this.objects.length > 0){
            for(var i=0; i< this.objects.length; i++){
                var curr_obj = this.objects[i];
                if(curr_obj.id == id){
                    return curr_obj;
                }
            }
    }

    return null;
};

LDService.prototype.render_row = function(click_obj) {
    if(typeof click_obj == "undefined" || click_obj == null){
        return;
    }

    var $data = $(click_obj).data();
    if(typeof $data == "undefined" || $data == null ||
        typeof $data.row_id == "undefined" || $data.row_id == null){
        return;
    }
    var obj = this.get_object_by_id($data.row_id);
    // console.log(obj);
    this.render_obj_details(obj);
};
