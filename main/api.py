import logging
from django.conf.urls import url
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout as django_logout
from django.contrib.auth.models import User
from django.middleware import csrf
from tastypie.http import HttpUnauthorized, HttpBadRequest
from tastypie.resources import ModelResource
from tastypie.utils import trailing_slash
from configurations.models import ContactInformation, Location, Company

from main.models import SeedListUser, LinkedInInfo, LinkedInConnection
from util.decorator import api_security_checks

logger = logging.getLogger(__name__)

ERR = {
    "status" : "ERROR",
    "error_message" : None,

}

SUCC = {'success': True}

class UserResource(ModelResource):  # pragma: no cover

    class Meta:
        queryset = User.objects.all()
        fields = ['first_name', 'last_name', 'email']
        allowed_methods = ['post']
        resource_name = 'user'

    def prepend_urls(self):
        return [

            url(r"^(?P<resource_name>%s)/update_from_linkedin%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('update_from_linkedin'), name="update_from_linkedin"),

            url(r"^(?P<resource_name>%s)/update_connections_from_linkedin%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('update_connections_from_linkedin'), name="update_connections_from_linkedin"),

            url(r"^(?P<resource_name>%s)/register%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('register'), name="register"),

            url(r"^(?P<resource_name>%s)/login%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('login'), name="login"),

            url(r'^(?P<resource_name>%s)/logout%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('logout'), name='logout'),
        ]

    def fetch_data(self, request):
        raw_data = None
        if hasattr(request, "raw_post_data"):
            raw_data = request.raw_post_data
        elif hasattr(request, "body"):
            raw_data = request.body

        if raw_data:
            return self.deserialize(request, raw_data, format=request.META.get('CONTENT_TYPE', 'application/json'))
        else:
            return {}


    @api_security_checks(allowed_methods = ['post'])
    def update_from_linkedin(self, request, *args, **kwargs):

        data = self.fetch_data(request)
        _total = data.get('_total')
        if _total != 1:
            error = ERR.copy()
            error['error_message'] = "more than 1 value in the body data"
            return self.create_response(request, error, HttpBadRequest)

        values = data.get('values')
        if not values or len(values) != 1:
            error = ERR.copy()
            error['error_message'] = "more invalid value or values count is grater than 1"
            return self.create_response(request, error, HttpBadRequest)

        linkedin_data = values[0]
        linkedin_id = linkedin_data.get('id')
        emailAddress = linkedin_data.get('emailAddress')

        try:
            user = SeedListUser.objects.get(linkedin_id = linkedin_id)
        except SeedListUser.DoesNotExist:
            if emailAddress:
                try:
                    user = SeedListUser.objects.get(username = emailAddress)
                except SeedListUser.DoesNotExist:
                    error = ERR.copy()
                    error['error_message'] = "no user with that linkedin-id / email [%s]/[%s] was found" % (linkedin_id, emailAddress)
                    return self.create_response(request, error, HttpBadRequest)
            else:
                error = ERR.copy()
                error['error_message'] = "no user with that linkedin-id [%s] was found, and no email was fetched from linkedin" % linkedin_id
                return self.create_response(request, error, HttpBadRequest)

        li_info = UserResource.get_or_create_info(linkedin_data)
        user.linkedin_info = li_info

        if emailAddress:
            user.email = emailAddress
            firstName = linkedin_data.get('firstName')
            lastName = linkedin_data.get('lastName')
            user.first_name = firstName
            user.last_name = lastName

            name = None
            if firstName and lastName:
                name = "%s %s" %(firstName, lastName)
            elif firstName:
                name = "%s" % firstName
            elif lastName:
                name = "%s" % lastName

            picture = linkedin_data.get('pictureUrl')
            linkedin_url = linkedin_data.get('publicProfileUrl')

            contact_defaults = {
                'name' : name,
                'linkedin_url' : linkedin_url,
                'picture' : picture,
            }

            contact_info, created = ContactInformation.objects.get_or_create(email = emailAddress, defaults = contact_defaults)
            if not created:
                contact_info.name = name
                contact_info.linkedin_url = linkedin_url
                contact_info.picture = picture
                contact_info.save()

            user.contact_info = contact_info

        else:
            logger.warn("no email address was found in the body data %s" % data)

        user.save()

        return self.create_response(request, SUCC)

    @staticmethod
    def get_or_create_info(linkedin_data):

        defaults = {
            'firstName' : linkedin_data.get('firstName'),
            'lastName' : linkedin_data.get('lastName'),
            'headline' : linkedin_data.get('headline'),
            'industry' : linkedin_data.get('industry'),
            'pictureUrl' : linkedin_data.get('pictureUrl'),
            'publicProfileUrl' : linkedin_data.get('publicProfileUrl'),
        }

        siteStandardProfileRequest = linkedin_data.get('siteStandardProfileRequest')
        if siteStandardProfileRequest:
            defaults['siteStandardProfileRequest'] = siteStandardProfileRequest.get('url')

        apiStandardProfileRequest = linkedin_data.get('apiStandardProfileRequest')
        if apiStandardProfileRequest:
            defaults['apiStandardProfileRequest'] = apiStandardProfileRequest.get('url')

        li_info, created = LinkedInInfo.objects.get_or_create(linkedin_id = linkedin_data.get('id'), defaults = defaults)

        if created:
            location = linkedin_data.get('location')
            if location:
                name = location.get('name')
                if name:
                    li_info.location = Location.objects.get_or_create(name=name.lower().strip())[0]
                    li_info.save()

            positions = linkedin_data.get('positions')
            if positions:
                values = positions.get('values')
                if values:
                    for value in values:
                        li_company = value.get('company')
                        if li_company:
                            name = li_company.get('name')
                            if name:
                                company, created = Company.objects.get_or_create(name = name.lower().strip())
                                li_info.positions.add(company)
        return li_info


    @api_security_checks(allowed_methods = ['post'])
    def update_connections_from_linkedin(self, request, *args, **kwargs):
        data = self.fetch_data(request)

        values = data.get('values')
        user_linkedin_id = data.get('user_linkedin_id')

        if user_linkedin_id:
            try:
                curr_user_info = LinkedInInfo.objects.get(linkedin_id=user_linkedin_id)
            except LinkedInInfo.DoesNotExist:
                error = ERR.copy()
                error['error_message'] = "could not fetch the current logged in user LinkedInInfo model from the db"
                return self.create_response(request, error, HttpBadRequest)
        else:
            error = ERR.copy()
            error['error_message'] = "could not fetch the current logged in user LinkedInInfo model from the db"
            return self.create_response(request, error, HttpBadRequest)

        if not values:
            error = ERR.copy()
            error['error_message'] = "more invalid value or values count is grater than 1"
            return self.create_response(request, error, HttpBadRequest)

        for linkedin_data in values:

            linkedin_id = linkedin_data.get('id')
            if not linkedin_id:
                continue

            li_info = UserResource.get_or_create_info(linkedin_data)
            LinkedInConnection.objects.get_or_create(linkedin_info_1 = curr_user_info, linkedin_info_2 = li_info)

        return self.create_response(request, SUCC)

    @api_security_checks(allowed_methods = ['post'])
    def login(self, request, *args, **kwargs):

        data = self.fetch_data(request)

        username = data.get('username')
        password = data.get('password')
        linkedin_id = data.get('linkedin_id')

        user = None

        if linkedin_id:
            try:
                # try to fetch the user by linkedin id
                user = SeedListUser.objects.get(linkedin_id=linkedin_id)
                # a bit of a hack..
                # see: http://stackoverflow.com/a/23771930
                user.backend = 'django.contrib.auth.backends.ModelBackend'

            except SeedListUser.DoesNotExist:
                # the user does not exist, create one
                return self.register_common(request, data)

        elif username is not None and password is not None:
            user = authenticate(username=username, password=password)

        if user and user.is_active:
            login(request, user)
            return self.create_response(request, {
                'success': True,
                'csrf': csrf.get_token(request),
                'sessionid': request.session.session_key
            })
        else:
            return self.create_response(request, {
                'success': False,
                'error_message': 'Authentication error. Please check the username/password',
            }, HttpUnauthorized)


    def register_common(self, request, data = None):

        if not data:
            data = self.fetch_data(request)

        username = data.get('username')
        password = data.get('password')
        linkedin_id = data.get('linkedin_id')

        user_defaults = {
            'is_staff': True,
            'is_superuser': True,
            'linkedin_id':linkedin_id,
        }

        if linkedin_id:
            # linked-in registration
            user, created = SeedListUser.objects.get_or_create(username=linkedin_id, defaults=user_defaults)
            if not created:
                return self.create_response(request, {
                    'success': False,
                    'error_message': 'user with the linked-in id %s already exists' % linkedin_id,
                }, HttpBadRequest)

            if user:
                # a bit of a hack..
                # see: http://stackoverflow.com/a/23771930
                user.backend = 'django.contrib.auth.backends.ModelBackend'
                login(request, user)
                return self.create_response(request, {
                    'success': True,
                    'csrf': csrf.get_token(request),
                    'sessionid': request.session.session_key
                })

            else:
                return self.create_response(request, {
                    'success': False,
                    'error_message': 'some unknown error has occurred, could not create a new user',
                }, HttpBadRequest)

        elif username is not None and password is not None:
            # username-password registration:
            user, created = SeedListUser.objects.get_or_create(username=username, defaults=user_defaults)

            if not created:
                return self.create_response(request, {
                    'success': False,
                    'error_message': 'user with the username %s already exists' % username,
                }, HttpBadRequest)

            if user:
                user.set_password(password)

                first_name = data.get('first_name')
                last_name = data.get('last_name')

                contact_info, created = ContactInformation.objects.get_or_create(email=username)
                contact_info.name = "%s %s" % (first_name, last_name)
                contact_info.save()

                user.contact_info = contact_info
                user.email = username
                user.first_name = first_name
                user.last_name = last_name
                user.save()

                user = authenticate(username=username, password=password)
                login(request, user)
                return self.create_response(request, {
                    'success': True,
                    'csrf': csrf.get_token(request),
                    'sessionid': request.session.session_key
                })

        else:
            return self.create_response(request, {
                'success': False,
                'error_message': 'could not register user, please provide either linkedin_id or (username,password) pair',
            }, HttpBadRequest)

    def register(self, request, *args, **kwargs):
        return self.register_common(request)


    @api_security_checks(allowed_methods = ['post'])
    def logout(self, request, *args, **kwargs):

        if request.user and request.user.is_authenticated():
            django_logout(request)
            return self.create_response(request, SUCC)
        else:
            return self.create_response(request, {'success': False}, HttpUnauthorized)