import json

from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.conf import settings

from configurations.models import ActivityLevel, InvestorKind


def get_base_context(full=True):
    context = {
        'LINKEDIN_API_KEY': settings.LINKEDIN_API_KEY,
        'API_VERSION': "/api/" + settings.TASTYPIE_API_NAME
    }
    if full:
        activity_level_dict = {key: val for key, val in ActivityLevel.ACTIVITY_LEVEL}
        context['ACTIVITY_LEVEL'] = json.dumps(activity_level_dict)
        kinds = list(InvestorKind.objects.values_list('name', flat=True))
        context['INVESTOR_KIND'] = json.dumps(kinds)
    return context

@login_required(login_url='/accounts/login/')
def main(request, *args, **kwargs):
    return render_to_response('main.html', get_base_context(full=False))


@login_required(login_url='/accounts/login/')
def investors(request, *args, **kwargs):
    return render_to_response('investors.html', get_base_context())


@login_required(login_url='/accounts/login/')
def vcs(request, *args, **kwargs):
    return render_to_response('vcs.html', get_base_context())


@login_required(login_url='/accounts/login/')
def accelerators(request, *args, **kwargs):
    return render_to_response('accelerators.html', get_base_context())


@login_required(login_url='/accounts/login/')
def journalists(request, *args, **kwargs):
    return render_to_response('journalists.html', get_base_context())


@login_required(login_url='/accounts/login/')
def startups(request, *args, **kwargs):
    return render_to_response('startups.html', get_base_context())


def login(request, *args, **kwargs):
    # TODO: login page: change color schema - to match main site
    return render_to_response('login.html', get_base_context(full=False))

def signup(request, *args, **kwargs):
    return render_to_response('signup.html', get_base_context(full=False))

