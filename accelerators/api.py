import logging

from tastypie import fields
from tastypie.constants import ALL, ALL_WITH_RELATIONS

from accelerators.models import AcceleratorProgram
from configurations.api import ContactInformationResource, CompanyResource, LocationResource, IndustryResource
from util.api import BaseResource


logger = logging.getLogger(__name__)


class AcceleratorProgramResource(BaseResource):
    company = fields.ToOneField(CompanyResource, 'company', full=True, null=True)

    contact_information = fields.ToManyField(ContactInformationResource, 'contact_information', full=True, null=True)
    location = fields.ToManyField(LocationResource, 'location', full=True, null=True)
    industry = fields.ToManyField(IndustryResource, 'industry', full=True, null=True)
    notable_alumni = fields.ToManyField(CompanyResource, 'notable_alumni', full=True, null=True)


    class Meta(BaseResource.Meta):
        queryset = AcceleratorProgram.objects.all().select_related('company') \
            .prefetch_related('contact_information', 'location', 'industry', 'notable_alumni')

        resource_name = 'accelerator_program'

        filtering = {
            'next_class': ALL,
            'number_of_startups_in_class_min': ALL,
            'number_of_startups_in_class_max': ALL,
            'duration_of_program': ALL,
            'application_deadline': ALL,
            'equity_take': ALL,
            'investment': ALL,
            'total_companies_graduated': ALL,

            'industry': ALL_WITH_RELATIONS,
            'notable_alumni': ALL_WITH_RELATIONS,
            'company': ALL_WITH_RELATIONS,
            'contact_information': ALL_WITH_RELATIONS,
            'location': ALL_WITH_RELATIONS,
        }

        ordering = ['next_class', 'number_of_startups_in_class_min', 'number_of_startups_in_class_max',
                    'duration_of_program', 'application_deadline', 'equity_take', 'investment',
                    'total_companies_graduated', 'industry', 'notable_alumni', 'company', 'contact_information',
                    'location', 'last_updated']
