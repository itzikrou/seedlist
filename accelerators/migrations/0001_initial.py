# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('configurations', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AcceleratorProgram',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('last_updated', models.DateTimeField(help_text=b'When was field last updated (updates automatically)', auto_now=True, auto_now_add=True)),
                ('next_class', models.DateTimeField(help_text=b'Next class', null=True, blank=True)),
                ('number_of_startups_in_class_min', models.IntegerField(default=0, help_text=b'Number of startups in each class: min')),
                ('number_of_startups_in_class_max', models.IntegerField(default=0, help_text=b'Number of startups in each class: max')),
                ('duration_of_program', models.CharField(help_text=b'Duration of program', max_length=1024, null=True, blank=True)),
                ('application_deadline', models.DateTimeField(help_text=b'Application deadline', null=True, blank=True)),
                ('equity_take', models.CharField(help_text=b'Equity taken', max_length=1024, null=True, blank=True)),
                ('investment', models.CharField(help_text=b'Investment', max_length=1024, null=True, blank=True)),
                ('total_companies_graduated', models.IntegerField(default=0, help_text=b'Total companies graduated')),
                ('company', models.ForeignKey(related_name='accel_company', to='configurations.Company', help_text=b'Accelerator name', null=True)),
                ('contact_information', models.ManyToManyField(help_text=b'Contact information', to='configurations.ContactInformation', null=True)),
                ('industry', models.ManyToManyField(help_text=b'Industry', to='configurations.Industry', null=True)),
                ('location', models.ManyToManyField(help_text=b'Location', to='configurations.Location', null=True)),
                ('notable_alumni', models.ManyToManyField(help_text=b'Notable alumni', related_name='notable_alumni', null=True, to='configurations.Company')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
    ]
