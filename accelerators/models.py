from django.db import models

from configurations.models import MAX_CHAR_LENGTH, Location, ContactInformation, \
    BaseModel, Company, Industry


class AcceleratorProgram(BaseModel):
    next_class = models.DateTimeField(null=True, blank=True, help_text="Next class")
    number_of_startups_in_class_min = models.IntegerField(default=0, help_text="Number of startups in each class: min")
    number_of_startups_in_class_max = models.IntegerField(default=0, help_text="Number of startups in each class: max")
    duration_of_program = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True, help_text="Duration of program")
    application_deadline = models.DateTimeField(null=True, blank=True, help_text="Application deadline")
    equity_take = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True, help_text="Equity taken")
    investment = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True, help_text="Investment")
    total_companies_graduated = models.IntegerField(default=0, help_text="Total companies graduated")

    company = models.ForeignKey(Company, null=True, related_name='accel_company', help_text="Accelerator name")
    contact_information = models.ManyToManyField(ContactInformation, null=True, help_text="Contact information")
    location = models.ManyToManyField(Location, null=True, help_text="Location")
    industry = models.ManyToManyField(Industry, null=True, help_text="Industry")
    notable_alumni = models.ManyToManyField(Company, null=True, related_name='notable_alumni', help_text="Notable alumni")


