from django.contrib import admin

from accelerators.models import AcceleratorProgram
from configurations.admin import BaseAdmin


class AcceleratorProgramAdmin(BaseAdmin):
    
    list_display = ('name', 'contact_information_custom', 'website', 'location_summary', 'next_class', 'number_of_startups_in_class_min',
                    'number_of_startups_in_class_max', 'duration_of_program', 'application_deadline', 'equity_take',
                    'investment', 'industry_list', 'notable_alumni_sum', 'total_companies_graduated')
    
    list_filter = ['location', 'number_of_startups_in_class_min', 'number_of_startups_in_class_max', 'equity_take']
    
    search_fields = ['company__name', 'contact_information__email', 'company__url']
    
    filter_horizontal = ('contact_information', 'location', 'industry', 'notable_alumni')
    
    readonly_fields = ('last_updated',)
    ordering = ('-last_updated',)
    
    def get_queryset(self, request):
        """
        Overloading of the Django default function - this is used in order to do select releated of the following models and reduce SQL queries
        """
        qs = super(AcceleratorProgramAdmin, self).get_queryset(request)\
        .prefetch_related("contact_information", 'location', 'industry', 'notable_alumni')\
        .select_related("company")
        
        return qs
    
    def name(self, obj):
        return obj.company.name
    
    name.short_description = 'Name'
    name.admin_order_field = 'company__name'
    
    def contact_information_custom(self, obj):
        
        emails = list(obj.contact_information.values_list("email", flat=True))
        if not any(emails):
            emails = []
        
        urls = list(obj.contact_information.values_list("url", flat=True))
        if not any(urls):
            urls = []
        
        data = emails + urls
        return " ".join(data)
    
    contact_information_custom.short_description = 'Contact information'
    contact_information_custom.admin_order_field = 'contact_information__email'

    def website(self, obj):
        return obj.company.url
    
    website.short_description = 'Website'
    website.admin_order_field = 'company__url'
    
    def location_summary(self, obj):
        location_list = obj.location.values_list("name", flat=True)
        return " ".join(location_list)
    
    location_summary.short_description = 'Location'
    location_summary.admin_order_field = 'location__name'
    
    def industry_list(self, obj):
        _industry_list = obj.industry.values_list("name", flat=True)
        return " ".join(_industry_list)

    industry_list.short_description = 'Industry'
    industry_list.admin_order_field = 'industry__name'

    
    def notable_alumni_sum(self, obj):
        _notable_alumni = obj.notable_alumni.values_list("name", flat=True)
        return " ".join(_notable_alumni)

    notable_alumni_sum.short_description = 'Notable Alumni'
    notable_alumni_sum.admin_order_field = 'notable_alumni__name'
    

admin.site.register(AcceleratorProgram, AcceleratorProgramAdmin)
