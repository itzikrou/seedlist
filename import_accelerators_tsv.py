#!/usr/bin/env python
import argparse
from datetime import datetime
import os

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "seedList.settings")
django.setup()


from accelerators.models import AcceleratorProgram
from configurations.models import ContactInformation, Company, Location, \
    Industry
    
def fetch_string(data):
    if data:
        return data.strip().lower()
    else:
        return data

def extract_contact_info(c_info, name):
    
    c_infos = []
    defaults = {
                "name":name
                }
    
    if c_info:
        
        if '@' in c_info:
            # email
            emails = []
            if ';' in c_info:
                c_info = c_info.split(';')
                for info in c_info:
                    if ':' in info:
                        emails.append(info.split(':')[1])
                    else:
                        emails.append(info)
            else:
                emails.append(c_info)        
            
            for email in emails:
                contact, created = ContactInformation.objects.get_or_create(email = fetch_string(email), defaults = defaults)
                c_infos.append(contact)
            
        elif 'http' in c_info:
            # web address
            contact, created = ContactInformation.objects.get_or_create(url = fetch_string(c_info), defaults = defaults)
            c_infos.append(contact)
    
    return c_infos

def parse_num_of_statups(number):
    result = (0,0)
    
    try:
        if number:
            if '-' in number:
                number = number.split('-')
                if len(number) == 2:
                    result =  (int(number[0]), int(number[1]))
            else:
                result = (int(number),int(number))
    except Exception:
        pass
    
    return result

def parse_datetime(date_time):
    date = None
    try:
        date = datetime(date_time)
    except Exception:
        pass

    return date

def parse_grads(total_grads):
    
    result = 0
    
    try:
        result = int(total_grads)
    except Exception:
        pass
    
    return result

def main(debug, fname):
    
    with open(fname) as f:
        for line in f:
            
            if debug:
                print line
            
            # TSV format
            splitted_line = line.split("\t")
            
            if debug:
                print splitted_line
                
            name = fetch_string(splitted_line[0])
            if name:
                
                name = unicode(name, errors='ignore')
                
                website = fetch_string(splitted_line[2])
                
                next_class = fetch_string(splitted_line[4])
                next_class = parse_datetime(next_class)
                
                number_of_startups = fetch_string(splitted_line[5])
                min_num, max_num = parse_num_of_statups(number_of_startups)
                
                duration = fetch_string(splitted_line[6])
                deadline = fetch_string(splitted_line[7])
                deadline = parse_datetime(deadline)
                
                equity = fetch_string(splitted_line[8])
                investment = fetch_string(splitted_line[9])
                
                total_grads = fetch_string(splitted_line[12])
                total_grads = parse_grads(total_grads)
                
                defaults = {
                            'url': website
                }
                
                company, created = Company.objects.get_or_create(name = fetch_string(name), defaults=defaults)
                
                defaults = {
                            'next_class' : next_class,
                            'number_of_startups_in_class_min': min_num,
                            'number_of_startups_in_class_max': max_num,
                            'duration_of_program': duration,
                            'application_deadline': deadline,
                            'equity_take': equity,
                            'investment': investment,
                            'total_companies_graduated': total_grads
                }

                
                accl, created = AcceleratorProgram.objects.get_or_create(company = company, defaults=defaults)
                
                contact_info = fetch_string(splitted_line[1])
                contact_infos = extract_contact_info(contact_info, name)
                if contact_infos:
                    for contact in contact_infos:
                        accl.contact_information.add(contact)
                        
                
                location_str = fetch_string(splitted_line[3])
                if location_str:
                    locations = location_str.split(",")
                    for location in locations:
                        loc, created =Location.objects.get_or_create(name = fetch_string(location))
                        accl.location.add(loc)
                    

                industry = fetch_string(splitted_line[10])
                if industry:
                    industries = industry.split(",")
                    for indu in industries:
                        ind, created = Industry.objects.get_or_create(name = fetch_string(indu))
                        accl.industry.add(ind)

                
                alums = fetch_string(splitted_line[11])
                if alums:
                    alumni = alums.split(",")
                    for alum in alumni:
                        comp, created = Company.objects.get_or_create(name = fetch_string(alum))
                        accl.notable_alumni.add(comp)
                    
                
            else:
                print "skipping %s, line is invalid" % splitted_line
            


# "main" function:
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='imports journalists tsv data into the project')
    parser.add_argument('-d', '--debug', dest='debug', action='store_true', default=False, help='print extra debug info')
    parser.add_argument('file_name', help='the journalists file to parse')
    args = parser.parse_args()
    main(args.debug, args.file_name)
    

