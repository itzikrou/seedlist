#!/usr/bin/env python
import argparse
import os

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "seedList.settings")
django.setup()

from configurations.models import ContactInformation, ActivityLevel, \
    ApproachTime, InvestorKind, Industry, Location, Entity, Company
from investors.models import Investor

def fetch_string(data):
    if data:
        return data.strip().lower()
    else:
        return data


def parse_amount(str_amount):
    int_val = 0
    
    str_amount = str_amount.replace('$', '').replace(',', '')
    
    try:
        int_val = int(str_amount)
    except Exception:
        pass
    
    return int_val

def main(debug, fname):
    
    def get_activity_key(activity_key):
        for key in ActivityLevel.activity_level_dict.keys():
            if key.lower() in activity_key:
                return key
        else:
            return None

    
    with open(fname) as f:
        for line in f:
            
            if debug:
                print line
            
            # TSV format
            splitted_line = line.split("\t")
            
            if debug:
                print splitted_line

            if not len(splitted_line) == 16:
                print "skipping line, its too short"
                continue
                
            invt_name = fetch_string(splitted_line[0])
            if invt_name:
                invt_name = unicode(invt_name, errors='ignore')
                email = fetch_string(splitted_line[10])
                if not email:
                    continue

                defaults = {
                            'name' :fetch_string(invt_name)
                }
                
                contact, created = ContactInformation.objects.get_or_create(email=fetch_string(email), defaults=defaults)
                
                activity_level_key = get_activity_key(fetch_string(splitted_line[9]))
                activity_level = ActivityLevel.activity_level_dict.get(activity_level_key, None)                
                
                defaults = {            
                    'min_investment' : parse_amount(fetch_string(splitted_line[7])),
                    'max_investment' : parse_amount(fetch_string(splitted_line[8])),
                    'activity_level' : activity_level,
                    'do_followons' : fetch_string(splitted_line[11]) in ["true", "True", "yes", "Yes", True],
                    'loved_companies' : fetch_string(splitted_line[13]),
                    'do_convertible_note' : fetch_string(splitted_line[14]) in ["true", "True", "yes", "Yes", True],
                    'guidelines' : fetch_string(splitted_line[15])
                }
                
                investor, created = Investor.objects.get_or_create(contact=contact, defaults=defaults)

                investor.activity_level = activity_level;
                
                activity = fetch_string(splitted_line[1])
                if activity:
                    companies = activity.split(",")
                    for company_name in companies:
                        company_name = unicode(company_name, errors='ignore')
                        company, created = Company.objects.get_or_create(name=fetch_string(company_name))
                        investor.activity.add(company)
                else:
                    print "activity is invalid: %s" % activity
                
                approach = fetch_string(splitted_line[2])
                if approach:
                    approach, created = ApproachTime.objects.get_or_create(name=fetch_string(approach))
                    investor.approach.add(approach)
                else:
                    print "approach is invalid: %s" % approach 
                
                entity = fetch_string(splitted_line[3])
                if entity:
                    entity = unicode(entity, errors='ignore')
                    entity, created = Entity.objects.get_or_create(name=fetch_string(entity))
                    investor.entity = entity
                else:
                    print "entity is invalid: %s" % entity
                
                kind = fetch_string(splitted_line[4])
                if kind:
                    kind, created = InvestorKind.objects.get_or_create(name=fetch_string(kind))
                    investor.kind.add(kind)
                else:
                    print "kind is invalid: %s" % approach
                
                primary_interest = fetch_string(splitted_line[5])
                if primary_interest:
                    primary_interest = primary_interest.split(",")
                    if primary_interest:
                        for ints in primary_interest:
                            primary_interest, created = Industry.objects.get_or_create(name=fetch_string(ints))
                            investor.primary_interest.add(primary_interest)
                    else:
                        print "primary_interest is invalid: %s" % approach 
                
                other_interests = fetch_string(splitted_line[6])
                if other_interests:
                    other_interests = other_interests.split(",")
                    if other_interests:
                        for ints in other_interests:
                            other_interests, created = Industry.objects.get_or_create(name=fetch_string(ints))
                            investor.other_interests.add(other_interests)
                    else:
                        print "other_interests is invalid: %s" % approach
                
                location = fetch_string(splitted_line[12])
                if location:
                    location = unicode(location, errors='ignore')
                    location, created = Location.objects.get_or_create(name=fetch_string(location))
                    investor.location.add(location)
                else:
                    print "location is invalid: %s" % approach

                investor.save()
                
            else:
                print "skipping %s, line is invalid" % splitted_line


# "main" function:
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='imports inverstors tsv data into the project')
    parser.add_argument('-d', '--debug', dest='debug', action='store_true', default=False, help='print extra debug info')
    parser.add_argument('file_name', help='the startups file to parse')
    args = parser.parse_args()
    main(args.debug, args.file_name)
    

