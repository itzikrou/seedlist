from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.auth.models import User, Group
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from tastypie.api import Api

from accelerators.api import AcceleratorProgramResource
from configurations.api import EtiquetteResource, IndustryResource, StageResource, ApproachTimeResource, \
    InvestorKindResource, EntityResource, ContactInformationResource, CompanyResource, \
    PositionResource, MediaResource, LocationResource
from investors.api import InvestorResource
from journalists.api import JournalistResource
from main.api import UserResource

from seedList.settings import TASTYPIE_API_NAME
from startups.api import StartupResource
from vcs.api import VCFundResource


v1_api = Api(api_name=TASTYPIE_API_NAME)
v1_api.register(IndustryResource())
v1_api.register(StageResource())
v1_api.register(ApproachTimeResource())
v1_api.register(InvestorKindResource())
v1_api.register(EntityResource())
v1_api.register(LocationResource())
v1_api.register(ContactInformationResource())
v1_api.register(CompanyResource())
v1_api.register(PositionResource())
v1_api.register(MediaResource())
v1_api.register(EtiquetteResource())

v1_api.register(InvestorResource())
v1_api.register(StartupResource())
v1_api.register(AcceleratorProgramResource())
v1_api.register(JournalistResource())
v1_api.register(VCFundResource())
v1_api.register(UserResource())

urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'seedList.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),

                       url(r'^accounts/login/', 'main.views.login', name='login'),
                       url(r'^signup/$', 'main.views.signup', name='signup'),

                       url(r'^$', 'main.views.main', name='main'),
                       url(r'^investors/', 'main.views.investors', name='investors'),
                       url(r'^vcs/', 'main.views.vcs', name='vcs'),
                       url(r'^accelerators/', 'main.views.accelerators', name='accelerators'),
                       url(r'^journalists/', 'main.views.journalists', name='journalists'),
                       url(r'^startups/', 'main.views.startups', name='startups'),
                       

                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^api/', include(v1_api.urls)),
)

urlpatterns += staticfiles_urlpatterns()

admin.site.unregister(User)
admin.site.unregister(Group)

admin.site.site_title = 'Seed-Lists'
admin.site.site_header = 'Seed-Lists'
admin.site.index_title = 'Seed-Lists'
