from django.contrib import admin

from configurations.admin import BaseAdmin
from journalists.models import Journalist


class JournalistAdmin(BaseAdmin):
    
    list_display = ('name', 'where', 'interested', 'email', 'mobile')
    
    list_filter = ['where']
    
    search_fields = ['contact__name', 'contact__email', 'where']
    
    readonly_fields = ('last_updated',)
    ordering = ('-last_updated',)
    
    def get_queryset(self, request):
        """
        Overloading of the Django default function - this is used in order to do select releated of the following models and reduce SQL queries
        """
        qs = super(JournalistAdmin, self).get_queryset(request).select_related("contact")
        
        return qs
    
    def name(self, obj):
        return obj.contact.name
    
    name.short_description = 'Name'
    name.admin_order_field = 'contact__name'

    def email(self, obj):
        return obj.contact.email
    
    email.short_description = 'Email'
    email.admin_order_field = 'contact__email'


    def mobile(self, obj):
        return obj.contact.mobile
    
    mobile.short_description = 'Mobile'
    mobile.admin_order_field = 'contact__mobile'

admin.site.register(Journalist, JournalistAdmin)
