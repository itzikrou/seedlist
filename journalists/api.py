import logging

from tastypie import fields
from tastypie.constants import ALL, ALL_WITH_RELATIONS

from configurations.api import ContactInformationResource, MediaResource
from journalists.models import Journalist
from util.api import BaseResource


logger = logging.getLogger(__name__)


class JournalistResource(BaseResource):
    contact = fields.ToOneField(ContactInformationResource, 'contact', full=True, null=True)
    where = fields.ToOneField(MediaResource, 'where', full=True, null=True)

    class Meta(BaseResource.Meta):
        queryset = Journalist.objects.all().select_related('contact', 'where')
        resource_name = 'journalist'

        filtering = {
            'interested': ALL,

            'where': ALL_WITH_RELATIONS,
            'contact': ALL_WITH_RELATIONS,
        }

        ordering = ['contact', 'where', 'interested', 'last_updated']
