from django.db import models

from configurations.models import ContactInformation, BaseModel, Media


class Journalist(BaseModel):    
    contact = models.ForeignKey(ContactInformation, null=True, help_text="Contact Deatils")
    where = models.ForeignKey(Media, null=True, help_text="Where")
    interested = models.TextField(blank=True, null=True, help_text="Intrested")
    
