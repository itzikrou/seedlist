#!/usr/bin/env python
import argparse
import os

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "seedList.settings")
django.setup()


from configurations.models import ContactInformation, Media
from journalists.models import Journalist


def fetch_string(data):
    if data:
        return data.strip().lower()
    else:
        return data


def main(debug, fname):
    
    with open(fname) as f:
        for line in f:
            
            if debug:
                print line
            
            # TSV format
            splitted_line = line.split("\t")
            
            if debug:
                print splitted_line
                
            name = fetch_string(splitted_line[0])
            if name:
                name = unicode(name, errors='ignore')
                email = fetch_string(splitted_line[3])
                mobile = fetch_string(splitted_line[4])
                
                defaults = {
                            'email': email,
                            'mobile': mobile
                }
                
                contact, created = ContactInformation.objects.get_or_create(name = fetch_string(name), defaults=defaults)
                
                interested = fetch_string(splitted_line[2])
                
                defaults = {
                            'interested': interested
                }
                
                journalist, created = Journalist.objects.get_or_create(contact=contact, defaults=defaults)
                
                where = fetch_string(splitted_line[1])
                if where:
                    media, created = Media.objects.get_or_create(name=fetch_string(where))
                    journalist.where = media
                    journalist.save()
                    
                
            else:
                print "skipping %s, line is invalid" % splitted_line
            


# "main" function:
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='imports journalists tsv data into the project')
    parser.add_argument('-d', '--debug', dest='debug', action='store_true', default=False, help='print extra debug info')
    parser.add_argument('file_name', help='the journalists file to parse')
    args = parser.parse_args()
    main(args.debug, args.file_name)
    

