import logging

from tastypie import fields
from tastypie.constants import ALL, ALL_WITH_RELATIONS

from configurations.api import CompanyResource, InvestorKindResource
from util.api import BaseResource
from vcs.models import VCFund


logger = logging.getLogger(__name__)


class VCFundResource(BaseResource):
    company = fields.ToOneField(CompanyResource, 'company', full=True, null=True)

    type = fields.ToManyField(InvestorKindResource, 'type', full=True, null=True)
    most_promising_portfolio_companies = fields.ToManyField(CompanyResource, 'most_promising_portfolio_companies',
                                                            full=True, null=True)

    class Meta(BaseResource.Meta):
        queryset = VCFund.objects.all().select_related('company') \
            .prefetch_related('type', 'most_promising_portfolio_companies')

        resource_name = 'vc_funds'

        filtering = {
            'contact_details': ALL,
            'number_of_deals_in_the_last_12_months': ALL,

            'company': ALL_WITH_RELATIONS,
            'type': ALL_WITH_RELATIONS,
            'most_promising_portfolio_companies': ALL_WITH_RELATIONS,

        }

        ordering = ['company', 'type', 'most_promising_portfolio_companies', 'contact_details',
                    'number_of_deals_in_the_last_12_months', 'last_updated']
