# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('configurations', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='VCFund',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('last_updated', models.DateTimeField(help_text=b'When was field last updated (updates automatically)', auto_now=True, auto_now_add=True)),
                ('number_of_deals_in_the_last_12_months', models.IntegerField(default=0, help_text=b'Number of deals in the last 12 months')),
                ('contact_details', models.TextField(help_text=b'', null=True, blank=True)),
                ('company', models.ForeignKey(related_name='vc_company', to='configurations.Company', help_text=b'VC fund name', null=True)),
                ('most_promising_portfolio_companies', models.ManyToManyField(help_text=b'', related_name='most_promising_portfolio_companies', null=True, to='configurations.Company')),
                ('type', models.ManyToManyField(help_text=b'Type / Status', to='configurations.InvestorKind', null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
    ]
