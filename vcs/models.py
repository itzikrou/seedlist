from django.db import models

from configurations.models import BaseModel, Company, \
    InvestorKind


class VCFund(BaseModel):
    company = models.ForeignKey(Company, null=True, related_name='vc_company', help_text="VC fund name")
    type = models.ManyToManyField(InvestorKind, null=True, help_text="Type / Status")
    number_of_deals_in_the_last_12_months = models.IntegerField(default=0, help_text="Number of deals in the last 12 months")
    most_promising_portfolio_companies = models.ManyToManyField(Company, null=True, related_name='most_promising_portfolio_companies', help_text="")
    contact_details = models.TextField(blank=True, null=True, help_text="")
