from django.contrib import admin

from configurations.admin import BaseAdmin
from vcs.models import VCFund


class VCFundAdmin(BaseAdmin):
    
    list_display = ('unesc_company', 'custom_type', 'number_of_deals_in_the_last_12_months', 'promising_portfolio_companies', 'contact_details')
    
    list_filter = ['type', 'number_of_deals_in_the_last_12_months']
    
    search_fields = ['company__name', 'type__name', 'most_promising_portfolio_companies__name']
    
    filter_horizontal = ('type', 'most_promising_portfolio_companies')
    
    readonly_fields = ('last_updated',)
    ordering = ('-last_updated',)
    
    def get_queryset(self, request):
        """
        Overloading of the Django default function - this is used in order to do select releated of the following models and reduce SQL queries
        """
        qs = super(VCFundAdmin, self).get_queryset(request).select_related("company").prefetch_related('type', 'most_promising_portfolio_companies')
        
        return qs
    
    def custom_type(self, obj):
        name_list = obj.type.values_list("name", flat=True)
        return " ".join(name_list)
    
    custom_type.admin_order_field = 'type__name'
    custom_type.short_description = 'Type / Status'

    def promising_portfolio_companies(self, obj):
        name_list = obj.most_promising_portfolio_companies.values_list("name", flat=True)
        return " ".join(name_list)
    
    promising_portfolio_companies.admin_order_field = 'most_promising_portfolio_companies__name'
    promising_portfolio_companies.short_description = 'Most Promising Portfolio companies'

    def unesc_company(self, obj):
        return obj.company.name
    
    unesc_company.allow_tags = True
    unesc_company.short_description = 'Name'
    unesc_company.admin_order_field = 'company__name'


admin.site.register(VCFund, VCFundAdmin)
