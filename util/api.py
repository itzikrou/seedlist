from tastypie.authorization import Authorization
from tastypie.resources import ModelResource


class BaseResource(ModelResource):
    class Meta:
        # TODO: change this!!
        # The no-op authorization option, no permissions checks are performed.
        authorization = Authorization()
        include_resource_uri = False