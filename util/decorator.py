from django.utils.decorators import available_attrs
from functools import wraps

def api_security_checks(allowed_methods=None):

    def api_wrapper(wrapped_function):

        @wraps(wrapped_function, assigned=available_attrs(wrapped_function))
        def _wrapped_api(self, request, *args, **kwargs):

            self.throttle_check(request)

            if allowed_methods:
                self.method_check(request, allowed=allowed_methods)

            return wrapped_function(self, request, *args, **kwargs)

        return _wrapped_api

    return api_wrapper