Seed-Lists
------------

Django admin interface on top the investors Google spread sheet.
-  see: https://docs.google.com/spreadsheets/d/1zuSO8fMBcju5S-ZfDh3ffajnn2U2eau40CbtBnp4ubA/edit#gid=392295388

=========================

This project is a Django admin project that uses Bootstrap.
-  see: https://github.com/django-admin-bootstrapped/django-admin-bootstrapped

Requirements
------------

-  Django ``>=1.7``

Installation
------------

For full django installation guide 
-  see: https://docs.djangoproject.com/en/1.7/intro/install/

1. Clone the repository

-  Git clone https://itzikrou@bitbucket.org/itzikrou/seedlist.git

-  For write permissions - ask me to add you to the repository contributers, email me at: itzikr@gmail.com

2. Set up a new virtual environment: virtualenv seedList

-  For full documentation see: http://virtualenv.readthedocs.org/en/latest/

3. Install pip dependencies: pip install -r requirements.txt

-  For full documentation see: https://pip.pypa.io/en/latest/

4. Set up your database configuration in a local_settings.py file (under seedList)

-  Run the migration scripts: manage.py migrate

-  Import the tsv data via the different import scripts (import_*.py) or use the handy bash script import_data.sh

5. Start the development server via manage.py runserver and start coding..

6. Have fun!

Configuration
-------------

Set up your database configuration in a local_settings.py file,
-  see: https://docs.djangoproject.com/en/dev/ref/settings/#databases

Homepage
~~~~~~~~

-  http://www.seedlists.com