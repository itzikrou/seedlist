# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('configurations', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Spammer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('last_updated', models.DateTimeField(help_text=b'When was field last updated (updates automatically)', auto_now=True, auto_now_add=True)),
                ('company', models.CharField(max_length=1024, null=True, blank=True)),
                ('what_they_were_pitching', models.CharField(help_text=b'', max_length=1024, null=True, blank=True)),
                ('contact', models.ForeignKey(to='configurations.ContactInformation', help_text=b'Contact Deatils', null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
    ]
