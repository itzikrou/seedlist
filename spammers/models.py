from django.db import models

from configurations.models import MAX_CHAR_LENGTH, ContactInformation, BaseModel


class Spammer(BaseModel):
    contact = models.ForeignKey(ContactInformation, null=True, help_text="Contact Deatils")
    company = models.CharField(max_length=MAX_CHAR_LENGTH,null=True, blank=True)    
    what_they_were_pitching = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True, help_text="")
