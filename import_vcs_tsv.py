#!/usr/bin/env python
import argparse
import os

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "seedList.settings")
django.setup()

from configurations.models import Company, InvestorKind
from vcs.models import VCFund


def fetch_string(data):
    if data:
        return data.strip().lower()
    else:
        return data


def main(debug, fname):
    
    with open(fname) as f:
        for line in f:
            
            if debug:
                print line
            
            # TSV format
            splitted_line = line.split("\t")
            
            if debug:
                print splitted_line
                
            name = fetch_string(splitted_line[0])
            if name:
                name = unicode(name, errors='ignore')
                company, created = Company.objects.get_or_create(name = fetch_string(name))
                
                num_of_deals = fetch_string(splitted_line[2])
                try:
                    num_of_deals = int(num_of_deals.replace("?", "0") if num_of_deals else "0")
                except ValueError:
                    num_of_deals = 0

                contact_details = fetch_string(splitted_line[4])
                
                defaults = {
                            'number_of_deals_in_the_last_12_months' :num_of_deals,
                            'contact_details' :contact_details
                }
                
                vc, created = VCFund.objects.get_or_create(company=company, defaults=defaults)

                type_status = fetch_string(splitted_line[1])
                if type_status:
                    contact, created = InvestorKind.objects.get_or_create(name=fetch_string(type_status))
                    vc.type.add(contact)
                
                most_promising_portfolio_companies = fetch_string(splitted_line[3])
                if most_promising_portfolio_companies:
                    splitted_comp = most_promising_portfolio_companies.split(",")
                    for comp in splitted_comp:
                        company, created = Company.objects.get_or_create(name = fetch_string(comp))
                        vc.most_promising_portfolio_companies.add(company)
                
            else:
                print "skipping %s, line is invalid" % splitted_line
            


# "main" function:
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='imports vcs tsv data into the project')
    parser.add_argument('-d', '--debug', dest='debug', action='store_true', default=False, help='print extra debug info')
    parser.add_argument('file_name', help='the vcs file to parse')
    args = parser.parse_args()
    main(args.debug, args.file_name)
    

