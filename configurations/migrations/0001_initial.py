# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ApproachTime',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Approach time', max_length=1024)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Company name', max_length=1024, null=True, blank=True)),
                ('url', models.CharField(help_text=b'Link to a web-page', max_length=1024, null=True, blank=True)),
                ('logo', models.CharField(help_text=b'company logo URL', max_length=10240, null=True, blank=True)),
            ],
            options={
                'verbose_name_plural': 'companies',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ContactInformation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Name', max_length=1024, null=True, blank=True)),
                ('email', models.EmailField(null=True, max_length=1024, blank=True, help_text=b'Email address', unique=True, db_index=True)),
                ('mobile', models.CharField(help_text=b'Mobile phone number', max_length=1024, null=True, blank=True)),
                ('url', models.CharField(help_text=b'Link to a web-page', max_length=1024, null=True, blank=True)),
                ('linkedin_url', models.CharField(help_text=b'Link to Linked-in profile', max_length=1024, null=True, blank=True)),
                ('crunchbase_url', models.CharField(help_text=b'Link to CrunchBase profile', max_length=1024, null=True, blank=True)),
                ('picture', models.CharField(help_text=b'profile picture URL', max_length=10240, null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Entity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Entity', max_length=1024)),
                ('url', models.CharField(help_text=b'Link to a web-page', max_length=1024, null=True, blank=True)),
            ],
            options={
                'verbose_name_plural': 'entities',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Etiquette',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('header', models.CharField(help_text=b'Etiquette panel header text', max_length=1024)),
                ('body', models.TextField(help_text=b'Etiquette panel body html')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Industry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Industry', max_length=1024)),
            ],
            options={
                'verbose_name_plural': 'industries',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='InvestorKind',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Investor kind', max_length=1024)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Location', max_length=1024)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Media',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Media (news paper, blog, site etc.) Name', max_length=1024)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Position',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Position', max_length=1024, null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Stage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Stage', max_length=1024)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
    ]
