# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

def enter_data(apps, schema_editor):
    Etiquette = apps.get_model("configurations", "Etiquette")
    header = 'Pitching Etiquette - Please read!'
    etq, created = Etiquette.objects.get_or_create(header = header)
    body = '''
    <p>This list is here to benefit companies looking for an investment. Here are some tips how to get the best out of it.</p>

    <p><b>IMPORTANT</b>: the last thing you want to do is send unsolicited, mass emails to these people.
    the likelihood of getting *any* response is low. instead, research what are their interest areas,
    and approach the appropriate ones, *individually*. otherwise, you are just ruining for everyone.
    removing contact info is just one click away for them.</p>

    <p>1. Before Pitching - Do your Research
    Understand your market and search the list for those investors who have shown interest in your market.</p>

    <p>2. Personalize your email pitch to the investor to show why you think he/she would be interested in this opportunity.</p>

    <p>3. Don't "spray and pray". Spamming the list will just make you a bad name. Remember we are in Israel and everyone knows everyone.</p>

    <p>4. All things equal people prefer to do business with people they know. Even if all things are not equal they still prefer to do business with people they know.
    The chance of you getting a meeting without a personal referral is much smaller. So do your research and find out who can connect you. Linkedin and Facebook are your friends</p>

    <p>5. Investors are people just like you. Don't waste their time. Be prepared with answers why you think they are a good fit, before you send the first email. Like said above do your research</p>

    <p>6. Get your name out there, so when you contact an investor there is a good chance he has heard about you.
    Go to meetups (there are a few every week), Talk to people, help other startups, in sum "get your name out there"</p>

    <p>7. Above all - Be honest. No one likes to invest in people who don't tell the truth and at the end of the day that is what the investor is doing - investing in people.</p>
    '''
    etq.body = body
    etq.save()

class Migration(migrations.Migration):

    dependencies = [
        ('configurations', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(enter_data),
    ]
