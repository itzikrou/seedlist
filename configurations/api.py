import logging
from django.conf import settings

from tastypie.constants import ALL

from configurations.models import Etiquette, Industry, Stage, ApproachTime, Entity, Location, ContactInformation, \
    Company, Position, Media, InvestorKind
from util.api import BaseResource


logger = logging.getLogger(__name__)


class IndustryResource(BaseResource):
    class Meta(BaseResource.Meta):
        queryset = Industry.objects.all()
        resource_name = 'industry'


class StageResource(BaseResource):
    class Meta(BaseResource.Meta):
        queryset = Stage.objects.all()
        resource_name = 'stage'

        filtering = {
            'name': ALL
        }

        ordering = ['name']


class ApproachTimeResource(BaseResource):
    class Meta(BaseResource.Meta):
        queryset = ApproachTime.objects.all()
        resource_name = 'approach_time'

        filtering = {
            'name': ALL
        }

        ordering = ['name']


class InvestorKindResource(BaseResource):
    class Meta(BaseResource.Meta):
        queryset = InvestorKind.objects.all()
        resource_name = 'investor_kind'

        filtering = {
            'name': ALL
        }

        ordering = ['name']


class EntityResource(BaseResource):
    class Meta(BaseResource.Meta):
        queryset = Entity.objects.all()
        resource_name = 'entity'

        filtering = {
            'name': ALL,
            'url': ALL
        }

        ordering = ['name']


class LocationResource(BaseResource):
    class Meta(BaseResource.Meta):
        queryset = Location.objects.all()
        resource_name = 'location'

        filtering = {
            'name': ALL
        }

        ordering = ['name']


class ContactInformationResource(BaseResource):
    class Meta(BaseResource.Meta):
        queryset = ContactInformation.objects.all()
        resource_name = 'contact_information'

        filtering = {
            'name': ALL,
            'email': ALL,
            'mobile': ALL,
            'url': ALL,
            'linkedin_url': ALL,
            'crunchbase_url': ALL
        }

        ordering = ['name', 'email', 'mobile']

    def dehydrate(self, bundle):
        # Include the request IP in the bundle.
        if not bundle.data['picture']:
            bundle.data['picture'] = "%s%s" % (settings.STATIC_URL, 'img/retina/no_contact_img.jpg')
        return bundle


class CompanyResource(BaseResource):
    class Meta(BaseResource.Meta):
        queryset = Company.objects.all()
        resource_name = 'company'

        filtering = {
            'name': ALL,
            'url': ALL
        }

        ordering = ['name']

    def dehydrate(self, bundle):
        # Include the request IP in the bundle.
        if not bundle.data['logo']:
            bundle.data['logo'] = "%s%s" % (settings.STATIC_URL, 'img/retina/generic_company_logo.png')
        return bundle


class PositionResource(BaseResource):
    class Meta(BaseResource.Meta):
        queryset = Position.objects.all()
        resource_name = 'position'

        filtering = {
            'name': ALL
        }

        ordering = ['name']


class MediaResource(BaseResource):
    class Meta(BaseResource.Meta):
        queryset = Media.objects.all()
        resource_name = 'media'

        filtering = {
            'name': ALL
        }

        ordering = ['name']


class EtiquetteResource(BaseResource):
    class Meta(BaseResource.Meta):
        queryset = Etiquette.objects.all()
        resource_name = 'etiquette'

        filtering = {
            'name': ALL
        }

        ordering = ['name']
