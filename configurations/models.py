from django.db import models


MAX_CHAR_LENGTH = 1024

class ActivityLevel(object):
    HIGH = 'High'
    MEDIUM = 'Medium'
    LOW = 'Low'
    NONE = 'Non-Existent'
    ACTIVITY_LEVEL = (
        (HIGH, 'High'),
        (MEDIUM, 'Medium'),
        (LOW, 'Low'),
        (NONE, 'Non-Existent'),
    )
    
    activity_level_dict = dict((y, x) for x, y in ACTIVITY_LEVEL)
    activity_level_lookup = dict((x, y) for x, y in ACTIVITY_LEVEL)
    
class BaseModel(models.Model):
    
    class Meta:
        abstract = True
        
    last_updated = models.DateTimeField(auto_now_add=True, auto_now=True, blank=True, help_text="When was field last updated (updates automatically)")


class BaseNameModel(models.Model):

    class Meta:
        abstract = True

    def __str__(self):
        return self.name

    def __unicode__(self):
       return u'%s' % self.name


class Industry(BaseNameModel):
    name = models.CharField(max_length=MAX_CHAR_LENGTH, help_text="Industry")
    
    class Meta:
        verbose_name_plural = "industries"
    
    
class Stage(BaseNameModel):   
    name = models.CharField(max_length=MAX_CHAR_LENGTH, help_text="Stage")

class ApproachTime(BaseNameModel):    
    name = models.CharField(max_length=MAX_CHAR_LENGTH, help_text="Approach time")
    
class InvestorKind(BaseNameModel):
    name = models.CharField(max_length=MAX_CHAR_LENGTH, help_text="Investor kind")

class Entity(BaseNameModel):
    name = models.CharField(max_length=MAX_CHAR_LENGTH, help_text="Entity")
    url = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True, help_text="Link to a web-page")
    
    class Meta:
        verbose_name_plural = "entities"
        

class Location(BaseNameModel):    
    name = models.CharField(max_length=MAX_CHAR_LENGTH, help_text="Location")

class ContactInformation(BaseNameModel):
    name = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True, help_text="Name")
    email = models.EmailField(max_length=MAX_CHAR_LENGTH, blank=True, null=True, help_text="Email address", db_index=True, unique=True)
    mobile = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True, help_text="Mobile phone number")
    url = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True, help_text="Link to a web-page")
    linkedin_url = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True, help_text="Link to Linked-in profile")
    crunchbase_url = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True, help_text="Link to CrunchBase profile")
    picture = models.CharField(max_length=(10 * MAX_CHAR_LENGTH), blank=True, null=True, help_text="profile picture URL")


class Company(BaseNameModel):
    name = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True, help_text="Company name")
    url = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True, help_text="Link to a web-page")
    logo = models.CharField(max_length=(10 * MAX_CHAR_LENGTH), blank=True, null=True, help_text="company logo URL")
    
    class Meta:
        verbose_name_plural = "companies"


class Position(BaseNameModel):   
    name = models.CharField(max_length=MAX_CHAR_LENGTH, blank=True, null=True, help_text="Position")
    
class Media(BaseNameModel):
    name = models.CharField(max_length=MAX_CHAR_LENGTH, help_text="Media (news paper, blog, site etc.) Name")
    
class Etiquette(models.Model):
    header = models.CharField(max_length=MAX_CHAR_LENGTH, help_text="Etiquette panel header text")
    body = models.TextField(help_text="Etiquette panel body html")

