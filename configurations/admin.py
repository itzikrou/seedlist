import locale

from django.contrib import admin

from configurations.models import Industry, ApproachTime, InvestorKind, Location, \
    ContactInformation, Company, Entity, Media, Etiquette


class BaseAdmin(admin.ModelAdmin):
    
    locale.setlocale( locale.LC_ALL, '' )
    list_per_page = 10
    list_select_related = True


class IndustryAdmin(admin.ModelAdmin):
        
    list_display = ('name',)
    
    list_filter = []
    
    search_fields = ['name']


class ApproachTimeAdmin(admin.ModelAdmin):
        
    list_display = ('name',)
    
    list_filter = []
    
    search_fields = ['name']


class InvestorKindAdmin(admin.ModelAdmin):
        
    list_display = ('name',)
    
    list_filter = []
    
    search_fields = ['name']

class LocationAdmin(admin.ModelAdmin):

    list_display = ('name',)
    
    list_filter = []
    
    search_fields = ['name']
    

class ContactInformationAdmin(admin.ModelAdmin):
    
    list_display = ('name', 'email', 'mobile', 'url', 'linkedin_url', 'crunchbase_url')
    
    list_filter = []
    
    search_fields = ['name', 'email', 'mobile', 'url']

class CompanyAdmin(admin.ModelAdmin):
        
    list_display = ('unesc_name', 'url')
    
    list_filter = []
    
    search_fields = ['name', 'url']
    
    def unesc_name(self, obj):
        return obj.name
    
    unesc_name.allow_tags = True
    unesc_name.short_description = 'Name'
    unesc_name.admin_order_field = 'name'
    
    


class EntityAdmin(admin.ModelAdmin):
        
    list_display = ('name', 'url')
    
    list_filter = []
    
    search_fields = ['name', 'url']    


class MediaAdmin(admin.ModelAdmin):

    list_display = ('name',)
    
    list_filter = []
    
    search_fields = ['name']
    
class EtiquetteAdmin(admin.ModelAdmin):
    list_display = ('unesc_header','unesc_body')
    
    def unesc_header(self, obj):
        return obj.header
    
    def unesc_body(self, obj):
        return obj.body

    
    unesc_header.allow_tags = True
    unesc_header.short_description = 'header'
    
    unesc_body.allow_tags = True
    unesc_body.short_description = 'body'


admin.site.register(ContactInformation, ContactInformationAdmin)
admin.site.register(Company, CompanyAdmin)
admin.site.register(Entity, EntityAdmin)
admin.site.register(Industry, IndustryAdmin)
admin.site.register(ApproachTime, ApproachTimeAdmin)
admin.site.register(InvestorKind, InvestorKindAdmin)
admin.site.register(Location, LocationAdmin)
admin.site.register(Media, MediaAdmin)
admin.site.register(Etiquette, EtiquetteAdmin)
    
