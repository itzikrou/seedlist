function etiquette_success_handler(data){	
	if(data.objects && data.objects.length > 0){
		var etiquette = data.objects[0];
		
		$("#etiquette_header").html(etiquette.header);
		$("#etiquette_body").html(etiquette.body);
		
	}
	
}

$(document).ready(function() {
	
	$.get("/api/v1/etiquette/?format=json", etiquette_success_handler)
})
