from django.db import models

from configurations.models import Industry, Stage, Position, \
    Company, BaseModel


class Compensation(BaseModel):
    company_field = models.ForeignKey(Industry, null=True, help_text="Company field")
    stage = models.ManyToManyField(Stage, null=True, help_text="Stage")
    num_of_employees = models.IntegerField(default=0, help_text="Number of employees at time of employ")
    position = models.ManyToManyField(Position, null=True, help_text="Position")
    has_income = models.BooleanField(default=False, help_text="Company has income?")
    equity = models.FloatField(default=0.0, help_text="equity % (of total outstanding)")
    salary = models.IntegerField(default=0, help_text="Optional: Salary")
    company = models.ManyToManyField(Company, null=True, help_text="Optional: Company name")
