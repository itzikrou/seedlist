# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('configurations', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Compensation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('last_updated', models.DateTimeField(help_text=b'When was field last updated (updates automatically)', auto_now=True, auto_now_add=True)),
                ('num_of_employees', models.IntegerField(default=0, help_text=b'Number of employees at time of employ')),
                ('has_income', models.BooleanField(default=False, help_text=b'Company has income?')),
                ('equity', models.FloatField(default=0.0, help_text=b'equity % (of total outstanding)')),
                ('salary', models.IntegerField(default=0, help_text=b'Optional: Salary')),
                ('company', models.ManyToManyField(help_text=b'Optional: Company name', to='configurations.Company', null=True)),
                ('company_field', models.ForeignKey(to='configurations.Industry', help_text=b'Company field', null=True)),
                ('position', models.ManyToManyField(help_text=b'Position', to='configurations.Position', null=True)),
                ('stage', models.ManyToManyField(help_text=b'Stage', to='configurations.Stage', null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
    ]
